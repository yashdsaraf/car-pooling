<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item"><a class="nav-link active" data-toggle="tab"
		href="#caredit" role="tab">Car</a></li>
	<li class="nav-item"><a class="nav-link" data-toggle="tab"
		href="#carrentaledit" role="tab">Car Rental</a></li>
	<li class="nav-item"><a class="nav-link" data-toggle="tab"
		href="#carpoolingedit" role="tab">Car Pooling</a></li>
	<li class="nav-item"><a class="nav-link" data-toggle="tab"
		href="#locationsedit" role="tab">Locations</a></li>
	<li class="nav-item"><a class="nav-link" data-toggle="tab"
		href="#ordersedit" role="tab">Orders</a></li>
</ul>
<div class="tab-content">
	<div class="tab-pane active" id="caredit" role="tabpanel">
		<button id="add_car" type="button" class="btn btn-primary btn-lg btn-block">Add Car</button>
		<table class="table table-responsive">
			<tbody>
			</tbody>
		</table>
		<br>
	</div>
	<div class="tab-pane" id="carrentaledit" role="tabpanel">
		<button id="add_rental" type="button" class="btn btn-primary btn-lg btn-block">Add Car Rental Service</button>
		<table class="table table-responsive">
			<tbody>
			</tbody>
		</table>
		<br>
	</div>
	<div class="tab-pane" id="carpoolingedit" role="tabpanel">
		<button id="add_pooling" type="button" class="btn btn-primary btn-lg btn-block">Add Car Pooling Service</button>
		<table class="table table-responsive">
			<tbody>
			</tbody>
		</table>
		<br>
	</div>
	<div class="tab-pane" id="locationsedit" role="tabpanel">
		<button id="add_location" type="button" class="btn btn-primary btn-lg btn-block">Add Location</button>
		<table class="table table-responsive">
			<tbody>
			</tbody>
		</table>
		<br>
	</div>
	<div class="tab-pane" id="ordersedit" role="tabpanel">
		<table class="table table-responsive">
			<tbody>
				<tr>
					<td scope="row" rowspan="4"><img src="" alt="Car Image"
						style="float: left;"></td>
					<td>Order Number:</td>
					<td colspan="5"></td>
					<td>Order Type:</td>
					<td></td>
					<td rowspan="2">
						<button type="button" class="btn btn-primary btn-sm">Modify
							Order</button>
					</td>
				</tr>
				<tr>
					<td>Customer Name:</td>
					<td colspan="5"></td>
					<td>Booking Date:</td>
					<td></td>
				</tr>
				<tr>
					<td>Car Name:</td>
					<td colspan="7"></td>
					<td rowspan="2">
						<button type="button" class="btn btn-primary btn-sm">Delete
							Order</button>
					</td>
				</tr>
				<tr>
					<td>Car Number:</td>
					<td colspan="77"></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<script src="assets/javascripts/employee.js"></script>
