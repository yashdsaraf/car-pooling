<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Transport Your World Pvt. Ltd.- Car pooling and rental
	service provider</title>

<link rel="stylesheet" href="assets/stylesheets/bootstrap.min.css">
<link rel="stylesheet" href="assets/stylesheets/animate.min.css">
<link rel="stylesheet"
	href="assets/fonts/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/stylesheets/main.css">
<link rel="stylesheet" href="assets/stylesheets/sweetalert2.min.css">

<script src="assets/javascripts/jquery-3.2.1.min.js"></script>

<div class="container-fluid">
	<div id="header">
		<nav id="header_nav"
			class="navbar fixed-top navbar-toggleable-sm navbar-light bg-faded">
			<button class="navbar-toggler navbar-toggler-right" type="button"
				data-toggle="collapse" data-target="#navbarNavAltMarkup">
				<span class="navbar-toggler-icon"></span>
			</button>
			<a id="brand_logo" class="navbar-brand" href="#">Transport your
				World</a>
			<!-- <a id="brand_logo" class="navbar-brand" href="#">Transport your World</a> -->
			<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
				<div class="nav navbar-nav ml-auto">
					<button id="contact_us" class="mr-2 mt-2 btn btn-outline-primary"
						type="button">Contact us</button>
					<button id="about_us" class="mr-2 mt-2 btn btn-outline-primary"
						type="button">About us</button>
					<c:choose>
						<c:when test="${sessionScope.currentUser == null}">
							<button id="register" class="mr-2 mt-2 btn btn-outline-success"
								type="button">Register</button>
							<button id="login" class="mr-2 mt-2 btn btn-outline-success"
								type="button">Login</button>
						</c:when>
						<c:otherwise>
							<form class="form-inline">
								<div class="nav-item dropdown ml-2 mr-5 mt-2">
									<a class="nav-link dropdown-toggle" href="#"
										data-toggle="dropdown"> Hi,
										${sessionScope.currentUser.fname} </a>
									<div class="dropdown-menu">
										<button class="btn dropdown-item" type="button">
											Action</button>
										<div class="dropdown-divider"></div>
										<button id="logout" class="btn dropdown-item" type="button">
											Logout</button>
									</div>
								</div>
							</form>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</nav>

	</div>
	<div id="main_body">
		<!-- BODY -->

		<c:choose>
			<c:when test="${sessionScope.currentUser == null}">
				<div class="container">
					<div id="carousel" class="carousel slide full-width-div"
						data-ride="carousel" data-interval="4000">
						<div class="carousel-inner" role="listbox">
							<div class="carousel-item active">
								<img class="d-block img-fluid" src="assets/images/home1.jpg"
									alt="First slide">
							</div>
							<div class="carousel-item">
								<img class="d-block img-fluid" src="assets/images/home3.jpg"
									alt="Third slide">
							</div>
							<div class="carousel-item">
								<img class="d-block img-fluid" src="assets/images/home2.jpg"
									alt="Second slide">
							</div>
						</div>
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<script>
					$('#external_css').remove()
				</script>

				<c:choose>
					<c:when test="${sessionScope.role.equals('customer') }">
						<link id="external_css" rel="stylesheet"
							href="assets/stylesheets/home.css">
						<c:import url="assets/html/home.html" />
					</c:when>
					<c:otherwise>
						<link id="external_css" rel="stylesheet"
							href="assets/stylesheets/employee.css">
						<c:import url="employee.jsp" />
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>

	</div>
	<i id="arrow_up" class="animated fa fa-angle-double-up"></i>
	<div id="footer">
		<nav
			class="navbar fixed-bottom navbar-toggleable-xl navbar-inverse bg-inverse">
			<button class="navbar-toggler navbar-toggler-right" type="button"
				data-toggle="collapse" data-target="#navbarNavAltMarkup2">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavAltMarkup2">
				<div class="nav navbar-nav">
					<span class="nav-item navbar-text"><i
						class="fa fa-copyright"></i> 2017 TransportYourWorld Pvt. Ltd.</span>
				</div>
				<div class="nav navbar-nav ml-auto">
					<a id="faq" class="nav-item nav-link" href="#">FAQ</a> <a
						id="canc_policy" class="nav-item nav-link" href="#">Cancellation
						policy</a>
				</div>
			</div>
		</nav>
	</div>
</div>

<script src="assets/javascripts/tether.min.js"></script>
<script src="assets/javascripts/bootstrap.min.js"></script>
<script src="assets/javascripts/sweetalert2.min.js"></script>
<script src="assets/javascripts/functions.js"></script>
<script src="assets/javascripts/index.js"></script>

<!-- Initialize Bootstrap functionality -->
<script>
	// Initialize tooltip component
	$(function() {
		$('[data-toggle="tooltip"]').tooltip()
	})

	// Initialize popover component
	$(function() {
		$('[data-toggle="popover"]').popover()
	})
</script>
