function _(id) {
    return document.getElementById(id)
}

function showSuccessSwal(title, text) {
    swal({
        title,
        text,
        showCancelButton: false,
        showConfirmButton: false,
        allowEscapeKey: true,
        allowOutsideClick: true,
        animation: true,
        timer: 3000,
        type: 'success'
    })
}

function showErrorSwal(title, text) {
    swal({
        title,
        text,
        showCancelButton: false,
        showConfirmButton: false,
        showCloseButton: true,
        allowEscapeKey: true,
        allowOutsideClick: true,
        animation: true,
        type: 'error'
    })
}

function getCookie(name) {
    let value = "; " + document.cookie
    let parts = value.split("; " + name + "=")
    if (parts.length == 2) return parts.pop().split(";").shift()
}

function checkCookie() {
    let cookieEnabled = navigator.cookieEnabled
    if (!cookieEnabled) {
        document.cookie = "__verify=1"
        if (document.cookie.indexOf("__verify=1") === -1)
            return false
        let thePast = new Date(1976, 8, 16)
        document.cookie = "__verify=1;expires=" + thePast.toUTCString()
    }
    return true
}

function checkLoggedIn() {
    if (document.cookie.indexOf('uid=') !== -1) {
        $.post('CheckSessionServlet', {
            uid: getCookie('uid')
        }, data => {
            if (data[0] == 'SUCCESS') {
                main_body.load('assets/html/home.html')
                    // $('#header').load('assets/html/header_' + data[1] +
                    // '.html')
                return true
            }
        })
    }
    return false
}

function validate(type, text) {
    switch (type) {
        case 'email':
            return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(text)
        case 'otp':
            return /^[0-9]{8}$/.test(text)
        case 'passwd':
            return /[a-zA-Z0-9!@#$%^&*?_-]{8,50}/.test(text)
        case 'num':
            return /^[1-9]{1}$/.test(text)
    }
}
