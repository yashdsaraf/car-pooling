let carlist = $('#car_list')

$(function() {
    $.post('GetFilterValsServlet', {
        type: 'rental'
    }, data => {
        if (data == "ERROR") return false
        let locArr = []
        let max = 0
        let passnum = getDropDownMenu();
        let cartype = getDropDownMenu();
        let carname = getDropDownMenu();
        let cartypeArr = [],
            carnameArr = []
        data.forEach(val => {
            locArr.push(val.town + ', ' + val.city + ', ' + val.state)
            max = val.no_of_passengers > max ? val.no_of_passengers : max
            getUniqueVals(cartypeArr, cartype, val.car_type)
            getUniqueVals(carnameArr, carname, val.car_name)
        })

        for (var i = 1; i <= max; i++) {
            passnum.append(getDropDownItem(i))
        }
        let resetDropDown = (id, elem) => {
            let target = $('#' + id).parent()
            target.find('.dropdown-menu').remove()
            target.append(elem)
        }
        resetDropDown('passnum_rental', passnum)
        resetDropDown('cartype_rental', cartype)
        resetDropDown('carname_rental', carname)
            // resetDropDown('bookdate_rental', bookdate)

        // Get unique elements from list
        locArr = locArr.filter((x, i, a) => a.indexOf(x) == i)

        $('#location_rental').typeahead({
            source: locArr,
            showHintOnFocus: true
        })
    })
    $.post('GetFilterValsServlet', {
        type: 'pooling'
    }, data => {
        if (data == "ERROR") return false
        let originArr = []
        let destinationArr = []
        let max = 0
        let passnum = getDropDownMenu();
        let cartype = getDropDownMenu();
        let carname = getDropDownMenu();
        let depdate = getDropDownMenu();
        let deptime = getDropDownMenu();
        let cartypeArr = [],
            carnameArr = [],
            depdateArr = [],
            deptimeArr = []
        data.forEach(val => {
            originArr.push(val.otown + ', ' + val.ocity + ', ' + val.ostate)
            destinationArr.push(val.town + ', ' + val.city + ', ' + val.state)
            max = val.no_of_passengers > max ? val.no_of_passengers : max
            getUniqueVals(depdateArr, depdate, val.depdate)
            getUniqueVals(deptimeArr, deptime, val.deptime)
            getUniqueVals(cartypeArr, cartype, val.car_type)
            getUniqueVals(carnameArr, carname, val.car_name)
        })
        for (var i = 1; i <= max; i++) {
            passnum.append(getDropDownItem(i))
        }
        let resetDropDown = (id, elem) => {
            let target = $('#' + id).parent()
            target.find('.dropdown-menu').remove()
            target.append(elem)
        }
        resetDropDown('passnum_pooling', passnum)
        resetDropDown('cartype_pooling', cartype)
        resetDropDown('carname_pooling', carname)
        resetDropDown('depdate_pooling', depdate)
        resetDropDown('deptime_pooling', deptime)

        // Get unique elements from list
        originArr = originArr.filter((x, i, a) => a.indexOf(x) == i)
        destinationArr = destinationArr.filter((x, i, a) => a.indexOf(x) == i)

        $('#origin_pooling').typeahead({
            source: originArr,
            showHintOnFocus: true
        })
        $('#destination_pooling').typeahead({
            source: destinationArr,
            showHintOnFocus: true
        })
    })
    $('.time-picker').hunterTimePicker()
    $('#search_rental').click()
})

$(document).on('click', '#search_rental', function() {
    let params = { order_type: 'rental' }
    checkAndAddIfExists(params, '#location_rental', 'location', true)
    checkAndAddIfExists(params, '#passnum_rental', 'passnum', false)
    checkAndAddIfExists(params, '#cartype_rental', 'type', false)
    checkAndAddIfExists(params, '#carname_rental', 'name', false)
    $.post('SearchCarsServlet', params, data => {
        carlist.empty()
        data.forEach(val => {
            let location = val.location
            let car = val.car
            let elem = `<tr>
                <td scope="row" rowspan="2"><img src="${car.carImage}" alt="Car Image" style="float: left;"></td>
                <td>Car Name:</td>
                <td class="table_cell">${car.carName}</td>
                <td>Car Type:</td>
                <td class="table_cell">${car.carType}</td>
                <td>Number of Passengers:</td>
                <td class="table_cell">${car.noOfPassengers}</td>
                <td rowspan="2">
                    <button id="${val.rentalId}" type="button" class="bookbtn_rental btn btn-primary btn-sm">Book</button>
                </td>
            </tr>
            <tr>
                <td>Location</td> 
                <td class="table_cell">${location.locality}, ${location.town}, ${location.district}, ${location.city}, ${location.state}</td>
                <td colspan="4"></td>
            </tr>`
            carlist.append(elem)
        })
    }).catch(() => {
        carlist.empty()
        alert('No cars found')
    })
})

$(document).on('click', '#search_pooling', function() {
    let params = { order_type: 'pooling' }
    checkAndAddIfExists(params, '#passnum_pooling', 'passnum', false)
    checkAndAddIfExists(params, '#cartype_pooling', 'type', false)
    checkAndAddIfExists(params, '#carname_pooling', 'name', false)
    checkAndAddIfExists(params, '#origin_pooling', 'origin', true)
    checkAndAddIfExists(params, '#destination_pooling', 'destination', true)
    checkAndAddIfExists(params, '#depdate_pooling', 'depdate', false)
    checkAndAddIfExists(params, '#deptime_pooling', 'deptime', false)
    $.post('SearchCarsServlet', params, data => {
        carlist.empty()
        data.forEach(val => {
            let origin = val.origin
            let destination = val.destination
            let car = val.car
            let elem = `<table class="table table-responsive">
    <tbody>
         <tr>
                 <td scope="row" rowspan="2"><img src="${car.carImage}" alt="Car Image" style="float: left;"></td>
                 <td>Car Name:</td>
                 <td class="table_cell">${car.carName}</td>
                 <td>Car Type:</td>
                 <td class="table_cell">${car.carType}</td>
                 <td>Number of Passengers:</td>
                 <td class="table_cell">${car.noOfPassengers}</td>
                 <td rowspan="2">
                     <button id="${val.poolingId}" type="button" class="bookbtn_pooling
                      btn btn-primary btn-sm">Book</button>
                 </td>
             </tr>
             <tr>
                 <td>Origin</td>
                 <td class="table_cell">${origin.locality}, ${origin.town}, ${origin.district}, ${origin.city}, ${origin.state}</td>
                 <td>Destination</td>
                 <td class="table_cell">${destination.locality}, ${destination.town}, ${destination.district}, ${destination.city}, ${destination.state}</td>
                 <td>Departure:</td>
                 <td class="table_cell">${val.departure}</td>
             </tr>
    </tbody>
                </table>`
            carlist.append(elem)
        })
    }).catch(() => {
        carlist.empty()
        alert('No cars found')
    })
})

$(document).on('click', '[class^="bookbtn_"]', function() {
    let order_type
    if ($(this).hasClass('bookbtn_rental'))
        order_type = 'rental'
    else if ($(this).hasClass('bookbtn_pooling'))
        order_type = 'pooling'
    else return false
    params = { order_type, id: this.id }
    $.post('GetBookingDetailsServlet', params, data => {
        if (data[0] == 'SUCCESS') {
            data = data[1]
            let html = `<table class="table table-responsive">
    <tbody>
        <tr>
            <th colspan=4> Customer details:- </th>
        </tr>
        <tr>
            <td> Name: </td>
            <td class="table_cell" colspan=3>${data.name}</td>
        </tr>
        <tr>
            <td> Email id: </td>
            <td class="table_cell" colspan=3>${data.email}</td>
        </tr>
        <tr>
            <td> Contact number: </td>
            <td class="table_cell" colspan=3>${data.phone}</td>
        </tr>
        <tr>
            <th colspan=4> Car details:- </th>
        </tr>
        <tr>
            <td> Car name: </td>
            <td class="table_cell" colspan=3>${data.carname}</td>
        </tr>
        <tr>
            <td> Car number:</td>
            <td class="table_cell">${data.carnum}</td>
            <td> Capacity: </td>
            <td class="table_cell">${data.passnum}</td>
        </tr>
        <tr>
            <td> Car type: </td>
            <td class="table_cell" colspan=3>${data.cartype}</td>
        </tr>
`
            if (order_type == 'pooling') {
                html += `<tr>
            <th colspan=4> Route details:- </th>
        </tr>
        <tr>
            <td>Origin: </td>
            <td class="table_cell" colspan=3>${data.origin}</td>
        </tr>
        <tr>
            <td>Destination: </td>
            <td class="table_cell" colspan=3>${data.destination}</td>
        </tr>`
            }
            html += `<tr>
            <th colspan=2> Fare: </th>
            <td class="table_cell" colspan=2>${data.fare}</td>
        </tr>
    </tbody>
</table>
`
            swal({
                title: '<u>Booking Confirmation</u>',
                html,
                showCloseButton: true,
                confirmButtonText: '<i class="fa fa-thumbs-up"></i> Confirm',
                cancelButtonText: ' <i class = "fa fa-thumbs-down"></i> Cancel',
                preConfirm: () => new Promise((resolve, reject) => {
                    $.post('BookCarServlet', params, data => {
                        if (data[0] == 'SUCCESS') {
                            $('#search_' + params.order_type).click()
                            resolve()
                        } else
                            showErrorSwal('Please try again', data[1])
                    })
                })
            }).then(() => {
                showSuccessSwal('Your order has been placed', '')
            }).catch(() => {})
        }
    })
})

$(document).on('shown.bs.tab', '[id^="tab"]', function() {
    $('#search_' + this.id.split('_')[1]).click()
})

function getUniqueVals(arr, elem, val) {
    if (!arr.includes(val)) {
        elem.append(getDropDownItem(val))
        arr.push(val)
    }
}

function getDropDownMenu() {
    let elem = $('<div></div>')
    elem.addClass('dropdown-menu')
    return elem
}

function getDropDownItem(val) {
    // let elem = document.createElement('a')
    let elem = $('<a></a>')
    elem.addClass('dropdown-item')
    elem.attr('href', '#')
    elem.text(val)
    return elem
}

function checkAndAddIfExists(params, id, name, isLocation) {
    let val
    if (!isLocation) {
        if (!$(id).parents('.dropdown').find('.dropdown-toggle').hasClass('selected'))
            return
        val = $(id).text()
    } else {
        val = $(id).val()
    }
    if (val !== null && val != '') {
        params[name] = val
    }
}
