let main_body = $('#main_body')

swal.setDefaults({
    allowEscapeKey: false,
    allowOutsideClick: false,
    showCancelButton: true,
    confirmButtonColor: 'blue',
    cancelButtonColor: 'red',
    confirmButtonClass: 'login_prompt',
    cancelButtonClass: 'login_prompt',
    animation: false,
    customClass: 'animated bounceIn',
    showLoaderOnConfirm: true
})

// $(document).on('click', '#alerttt', () => {
// if ($('#alert_card').is(':visible'))
// $('#alert_card').hide()
// else
// $('#alert_card').show()
// })
// <
//div id="alert_card" class="alert alert-danger" role="alert">
// <strong>Oh snap!</strong> Change a few things up and try submitting again.
// </div>
let regDetails = {}
let steps = [{
    title: '<h2>Register</h2>',
    html: `<small>Enter your details:</small><br><br>
            <div class="input-group">
                <i class="input-group-addon fa fa-user">&nbsp*</i>
                <input type="text" class="form-control" id="first_name" placeholder="First Name">
                <input type="text" class="form-control" id="last_name" placeholder="Last Name">
            </div><br>
            <div class="input-group">
                <div class="input-group-addon fa fa-phone">&nbsp*</div>
                <div class="input-group-addon">+91</div>
                <input type="number" class="form-control" id="phone_no" placeholder="Phone Number">
            </div><br>
            <div class="input-group">
                <i class="input-group-addon fa fa-envelope">&nbsp*</i>
                <input type="text" class="form-control" id="email_id" placeholder="Email ID">
            </div><br>
            <div class="input-group">
                <i class="input-group-addon fa fa-id-card-o"></i>
                <input type="text" class="form-control" id="license_no" placeholder="License Number">
            </div><br>
            <div class="input-group">
                <i class="input-group-addon fa fa-address-book"></i>
                <textarea class="form-control" id="address" placeholder="Address"></textarea>
            </div><br>
            <small><strong>Note: </strong> Fields marked with <i class="fa fa-asterisk"></i> are mandatory.</small><br><br>`,

    preConfirm: () => {
        regDetails = {
            fname: $('#first_name').val(),
            lname: $('#last_name').val(),
            phone: $('#phone_no').val(),
            email: $('#email_id').val(),
            license: $('#license_no').val(),
            address: $('#address').val()
        }
        return new Promise((resolve, reject) => {
            $.post('ValidateServlet', regDetails, data => {
                if (data[0] == 'ERROR') reject(data[1])
                else resolve()
            })
        })
    },
    progressSteps: ['1', '2'],
    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Submit',
    cancelButtonText: '<i class="fa fa-times"></i> Cancel'
}, {
    title: '<h2>Register</h2>',
    html: `<small>Set your login credentials:</small><br><br>
            <div class="input-group">
                <i class="input-group-addon fa fa-user-circle"></i>
                <input type="text" class="form-control" id="reg_uid" placeholder="User Name">
            </div><br>
                <div class="input-group">
                <i class="input-group-addon fa fa-lock"></i>
                <input type="password" class="form-control" id="reg_passwd" placeholder="Password">
            </div>
            <br><small>*Both should be minimum 8 characters long.</small><br><br>`,

    preConfirm: () => {
        return new Promise((resolve, reject) => {
            let loginDetails = {
                login_id: $('#reg_uid').val(),
                passwd: $('#reg_passwd').val()
            }
            $.post('ValidateServlet', loginDetails, data => {
                if (data[0] == 'ERROR') reject(data[1])
                else {
                    $.extend(regDetails, loginDetails)
                    $.post('RegisterServlet', regDetails, data => {
                        if (data[0] == 'ERROR') reject(data[1])
                        else resolve()
                    })
                }
            })
        })
    },
    progressSteps: ['1', '2'],
    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Submit',
    cancelButtonText: '<i class="fa fa-times"></i> Cancel'
}]

$(document).ready(() => {

    let arrow_up = $('#arrow_up')

    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            arrow_up.removeClass('animated slideOutDown')
            arrow_up.addClass('animated slideInUp')
            arrow_up.show()
        } else {
            arrow_up.removeClass('animated slideInUp')
            arrow_up.addClass('animated slideOutDown')
        }
    })

    arrow_up.click(function() {
            $('html, body').animate({ scrollTop: 0 }, 800);
            return false
        })
        // if (!checkCookie()) {
        // swal({
        // title: 'Please enable cookies to proceed',
        // html: `<p><small>We will never use cookies to share your personal
        // information.</small></p>`,
        // type: 'error',
        // showCancelButton: false,
        // confirmButtonText: 'Reload',
        // }).then((confirm) => {
        // if (confirm) {
        // location.reload()
        // }
        // })
        // }
        // if (!checkLoggedIn) $('#header').load('assets/html/header.html')
        // $('#footer').load('assets/html/footer.html')
})

$(document).on('click', '#about_us, #contact_us, #faq, #canc_policy', function() {
    $('#external_css').remove()
    main_body.parent().parent().append('<link id="external_css" rel="stylesheet" href="assets/stylesheets/' + this.id + '.css">')
    main_body.load('assets/html/' + this.id + '.html')
})

$(document).on('click', '#brand_logo', function() {
    location.reload()
})

$(document).on('click', '#login', function() {
    swal({
        title: '<h2>Login</h2>',
        html: `<small>Enter your login credentials:</small><br><br>
                <div class="input-group">
                <i class="input-group-addon fa fa-user-circle"></i>
                <input type="text" class="form-control" id="login_uid" placeholder="User Name">
            </div><br>
            <div class="input-group">
                <i class="input-group-addon fa fa-lock"></i>
                <input type="password" class="form-control passwd" id="login_passwd" placeholder="Password">   
                <button class="show_hide input-group-addon fa fa-eye" type="button"></button>
            </div><br>
            <div class="form-group">
                <div class="form-check">
                <label class="form-check-label">
                    <input id="role" class="form-check-input" type="checkbox"> <ins>Login as an employee</ins>
                </label>
                </div>
            </div>
            <a id="forgotpasswd" href="#"><small>Forgot password</small></a><br><br>`,
        preConfirm: () => {
            return new Promise((resolve, reject) => {
                let role = _('role').checked ? 'employee' : 'customer'
                $.post('LoginServlet', {
                    login_id: $('#login_uid').val(),
                    passwd: $('#login_passwd').val(),
                    role
                }, data => {
                    if (data[0] == 'ERROR') reject(data[1])
                    else resolve()
                })
            })
        },
        confirmButtonText: '<i class="fa fa-thumbs-up"></i> Submit',
        cancelButtonText: '<i class="fa fa-times"></i> Cancel'
    }).then(() => {
        // checkLoggedIn()
        location.reload()
    }).catch(() => {})
})

$(document).on('click', '#register', function() {
    swal.queue(steps).then(() => {
        // checkLoggedIn()
        location.reload()
    }).catch(() => {})
})

$(document).on('click', '#logout', function(e) {
    e.preventDefault()
    swal({
        type: 'warning',
        text: 'Are you sure you want to log out?',
        allowOutsideClick: true,
        allowEscapeKey: true
    }).then(() => {
        $.post('LogoutServlet', () => {
            location.reload()
        })
    }).catch(() => {})
})

$(document).on('click', '#forgotpasswd', function() {
    // swal.close()
    // main_body.load('ForgotPasswd.jsp')
    let email;
    let swalQueue = [{
        progressSteps: ['1', '2'],
        html: `<big>Enter your registered email address</big><br><br>
                <div class="form-group row">
                    <input class="form-control" type="email"
                        placeholder="user@example.com" id="forgotpasswd_email">
                </div><br>`,
        preConfirm: () => {
            email = $('#forgotpasswd_email').val()
            return new Promise((resolve, reject) => {
                if (email.length == 0)
                    reject('Email address cannot be left blank')
                else if (!validate('email', email))
                    reject('Please enter a valid email address')
                else
                    $.post('ForgotPasswdServlet', {
                        email
                    }, data => {
                        if (data[0] == 'ERROR') reject(data[1])
                        else resolve()
                    })
            })
        }
    }, {
        progressSteps: ['1', '2'],
        html: `<big>Enter OTP</big><br><br>
                <div class="form-group row">
                    <input class="form-control" type="number" placeholder="12345678"
                        id="forgotpasswd_otp">
                </div>
                <small>The otp is valid for 10 minutes only.</small><br><br>
                <big>Set your new password</big><br><br>
                <div class="input-group">
                <i class="input-group-addon fa fa-lock"></i>
                <input type="password" class="form-control passwd" id="forgotpasswd_passwd" placeholder="Password">   
                <button class="show_hide input-group-addon fa fa-eye" type="button"></button>
            </div><br><br>`,
        preConfirm: () => {
            let otp = $('#forgotpasswd_otp').val()
            let passwd = $('#forgotpasswd_passwd').val()
            return new Promise((resolve, reject) => {
                if (otp.length == 0)
                    reject('OTP cannot be left blank')
                else if (passwd.length == 0)
                    reject('Password cannot be left blank')
                else if (!validate('otp', otp))
                    reject('Please enter a valid OTP')
                else if (!validate('passwd', passwd))
                    reject('Please enter a valid password')
                else
                    $.post('ForgotPasswdServlet', {
                        otp,
                        passwd,
                        email
                    }, data => {
                        if (data[0] == 'ERROR') showErrorSwal(data[1], '')
                        else resolve()
                    })
            })
        }
    }]
    swal.queue(swalQueue).then(() => {
        showSuccessSwal('Your password has been reset', '')
    }).catch(() => {})
})

$(document).on('click', '.show_hide', function() {
    $(this).toggleClass('fa-eye fa-eye-slash')
    let pass = $('.passwd')
    if (pass.attr('type') == 'password')
        pass.attr('type', 'text')
    else
        pass.attr('type', 'password')
    pass.focus()
})

$(document).on('click', '#submit', function() {
    swal({
        type: 'success',
        title: 'Thank you!',
        text: "Your feedback has been sent.",
        timer: 2000,
        showCancelButton: false,
        allowOutsideClick: true
    })
})

$(document).on('click', '.dropdown-menu a', function() {
    let val = $(this).text()
    let elem = $(this).parents('.dropdown').find('.dropdown-toggle')
    elem.html(val + '<i class="caret"></i>')
    elem.addClass('selected')
})
