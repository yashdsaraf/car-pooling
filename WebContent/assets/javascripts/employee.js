$(function() {
    loadCarsIntoDiv()
    loadLocationsIntoDiv()
    loadCarRentalsIntoDiv()
    loadCarPoolsIntoDiv()
})

function loadCarsIntoDiv() {
    let elem = $('#caredit').find('tbody')
    $.post('GetAllCarsServlet', data => {
        if (!data.length) {
            return false;
        }
        elem.empty()
        data.forEach(car => {
            let row = `<tr>
    <td scope="row" rowspan="2"><img src="${car.carImage}" alt="Car Image"
        style="float: left;"></td>
        <td>Car Name</td>
    <td class="table_cell">${car.carName}</td>
    <td>Car Type</td>
    <td class="table_cell">${car.carType}</td>
    <td>Number of Passengers</td>
    <td class="table_cell">${car.noOfPassengers}</td>
    <td>
        <button id="${car.carNumber}" type="button" class="btn btn-primary btn-sm delete_car">Delete
            Car</button>
    </td>
</tr>
<tr>
    <td>Availability</td>
    <td class="table_cell">${car.availability}</td>
    <td>Order type</td>
    <td class="table_cell">${car.orderType}</td>
    <td>Car number</td>
    <td class="table_cell">${car.carNumber}</td>
    <td>
        <button type="button" class="btn btn-primary btn-sm modify_passnum">Modify
            Passengers</button>
    </td>
</tr>`
            elem.append(row)
        })
    })
}


function loadLocationsIntoDiv() {
    let elem = $('#locationsedit').find('tbody')
    $.post('GetAllLocationsServlet', data => {
        if (!data.length) {
            return false;
        }
        elem.empty()
        data.forEach(location => {
            let row = `
                <tr>
                    <td>State</td>
                    <td colspan="2" class="table_cell">${location.state}</td>
                    <td>City</td>
                    <td class="table_cell">${location.city}</td>
                    <td>
                        <button type="button" id="${location.locationId}" class="btn btn-primary btn-sm delete_location">Delete</button>
                    </td>
                </tr>
                <tr>
                    <td>District</td>
                    <td class="table_cell">${location.district}</td>
                    <td>Town</td>
                    <td class="table_cell">${location.town}</td>
                    <td>Locality</td>
                    <td class="table_cell">${location.locality}</td>
                </tr>
            `
            elem.append(row)
        })
    })
}

function loadCarRentalsIntoDiv() {
    let elem = $('#carrentaledit').find('tbody')
    $.post('SearchCarsServlet', { order_type: 'rental' }, data => {
        if (!data.length) {
            return false;
        }
        elem.empty()
        data.forEach(rent => {
            let row = `
                <tr>
                    <td>ID</td>
                    <td class="table_cell">${rent.rentalId}</td>
                    <td>Car number</td>
                    <td class="table_cell">${rent.carNumber}</td>
                    <td>Car name</td>
                    <td class="table_cell">${rent.car.carName}</td>
                    <td rowspan="2"><button type="button" id="${rent.rentalId}" class="btn btn-primary btn-sm delete_rental">Delete</button></td>
                </tr>
                <tr>
                    <td>Location</td>
                    <td class="table_cell" colspan="3">${rent.location.state}, ${rent.location.city}, ${rent.location.district}, ${rent.location.town}, ${rent.location.locality}</td>
                    <td>Fare</td>
                    <td class="table_cell">${rent.fare}</td>
                </tr>
            `
            elem.append(row)
        })
    })
}

function loadCarPoolsIntoDiv() {
    let elem = $('#carpoolingedit').find('tbody')
    $.post('SearchCarsServlet', { order_type: 'pooling' }, data => {
        if (!data.length) {
            return false;
        }
        elem.empty()
        data.forEach(pool => {
            let row = `
                <tr>
                    <td>ID</td>
                    <td class="table_cell">${pool.poolingId}</td>
                    <td>Car number</td>
                    <td class="table_cell">${pool.carNumber}</td>
                    <td>Car name</td>
                    <td class="table_cell">${pool.car.carName}</td>
                    <td>Departure</td>
                    <td class="table_cell" colspan="2">${pool.departure}</td>
                </tr>
                <tr>
                    <td>Origin</td>
                    <td class="table_cell" colspan="3">${pool.origin.state}, ${pool.origin.city}, ${pool.origin.district}, ${pool.origin.town}, ${pool.origin.locality}</td>
                    <td>Destination</td>
                    <td class="table_cell">${pool.destination.state}, ${pool.destination.city}, ${pool.destination.district}, ${pool.destination.town}, ${pool.destination.locality}</td>
                    <td>Fare</td>
                    <td class="table_cell">${pool.fare}</td>
                    <td><button type="button" id="${pool.poolingId}" class="btn btn-primary btn-sm delete_pooling">Delete</button></td>
                </tr>
            `
            elem.append(row)
        })
    })
}

// MODIFY PASSENGERS
$(document).on('click', '.modify_passnum', function() {
    carnum = $(this).parent().prev().text()
    swal({
        title: 'Enter number of passengers',
        type: 'question',
        html: '<input id="passnum_input" type="number"><br><br>',
        preConfirm: () => {
            return new Promise((resolve, reject) => {
                if (!validate('num', $('#passnum_input').val())) {
                    reject('Invalid no. of passengers')
                } else {
                    $.post('ModifyPassNumServlet', { num: $('#passnum_input').val(), carnum }, data => {
                        if (data[0] != 'SUCCESS')
                            showErrorSwal('', data[1])
                    })
                    loadCarsIntoDiv()
                    resolve()
                }
            })
        }
    }).then(() => { loadCarsIntoDiv() })
})

// DELETE CAR
$(document).on('click', '.delete_car', function() {
    if (!confirm('Are you sure you want to delete this car?'))
        return false
    $.post('DeleteCarServlet', { carnum: this.id }, data => {
        if (data[0] != 'SUCCESS') {
            showErrorSwal('', data[1])
            return false
        }
        loadCarsIntoDiv()
    })
})

// ADD NEW CAR
$(document).on('click', '#add_car', function() {
    swal({
        title: '<h2>Add New Car</h2>',
        html: `
        <form class="form-group">
        <p>Car Name:</p>
        <input id="car_name_input" class="form-control" type="text"><br>
        <p>Car Type:</p>
        <div class="dropdown mb-2 mr-sm-2 mb-sm-0">
            <a id="car_type_input" href="#" class="btn btn-secondary btn-block dropdown-toggle" data-toggle="dropdown" role="button">HATCHBACK
<i class="caret"></i></a>
            <div class="dropdown-menu">
            <a href="#" class="dropdown-item btn btn-block">HATCHBACK</a>
            <a href="#" class="dropdown-item btn btn-block">SEDAN</a>
            <a href="#" class="dropdown-item btn btn-block">WAGON</a>
            <a href="#" class="dropdown-item btn btn-block">SUV</a>
            </div>
        </div>
        <br>
        <p>Car Number:</p>
        <input id="car_num_input" class="form-control" type="text"><br>
        <p>Order Type:</p>
        <div class="dropdown mb-2 mr-sm-2 mb-sm-0">
            <a id="car_ordertype_input" href="#" class="btn btn-secondary btn-block dropdown-toggle" data-toggle="dropdown" role="button">RENT
<i class="caret"></i></a>
            <div class="dropdown-menu">
            <a href="#" class="dropdown-item btn btn-block">POOL</a>
            <a href="#" class="dropdown-item btn btn-block">RENT</a>
            </div>
        </div>
        <br>
        <p>No. of passengers:</p>
        <input id="car_passnum_input" class="form-control" type="number"><br>
        <p>Car image:</p>
        <input id="car_img_input" class="form-control" type="file" id="file" accept=".jpg,.jpeg"><br>
        </form>
`,
        preConfirm: () => new Promise((resolve, reject) => {
            let form_data = new FormData();
            let image = $('#car_img_input').prop('files')[0]
            form_data.append('image', image)
            form_data.append('name', $('#car_name_input').val())
            form_data.append('type', $('#car_type_input').text())
            form_data.append('order', $('#car_ordertype_input').text())
            form_data.append('passnum', $('#car_passnum_input').val())
            form_data.append('carnum', $('#car_num_input').val())

            $.ajax({
                type: 'POST',
                url: 'AddCarServlet',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                success: data => {
                    if (data[0] == 'SUCCESS')
                        resolve()
                    else
                        showErrorSwal('', data[1])
                }
            })
        }),
    }).then(() => {
        showSuccessSwal('', 'New car has been added successfully!')
        loadCarsIntoDiv()
    }).catch(() => {})
})

// DELETE CAR RENTAL SERVICE
$(document).on('click', '.delete_rental', function() {
    if (!confirm('Are you sure you want to delete this service?'))
        return false
    $.post('DeleteServiceServlet', { rental_id: this.id }, data => {
        if (data[0] != 'SUCCESS') {
            showErrorSwal('', data[1])
            return false
        }
        loadCarRentalsIntoDiv()
    })
})

// ADD CAR RENTAL SERVICE
$(document).on('click', '#add_rental', function() {
            let makeAjaxCall = (locArr, carArr) => {
                    swal({
                                width: '70%',
                                height: '80%',
                                title: '<h2>Add Car Rental Service</h2>',
                                html: `<div class="form-group" style="text-align: left">
                                <div class="dropdown">
                                    <a id="add_rental_location" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#" role="button">Location<i class="caret"></i></a>
                                    <div class="dropdown-menu">
                                        ${locArr.map(loc => `<a class="dropdown-item" href="#">${loc.locationId} - ${loc.state}, ${loc.city}, ${loc.district}, ${loc.town}, ${loc.locality}</a>`).join("")}
                                    </div>
                                </div><br>
                                <div class="dropdown">
                                    <a id="add_rental_car" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#" role="button">Car</a>
                                    <div class="dropdown-menu">
                                        ${carArr.map(car => `<a class="dropdown-item" href="#">${car.carNumber} - ${car.carName}</a>`).join("")}
                                    </div>
                                </div><br>
                                <div class="form-inline">
                                Fare:
                                <input id="add_rental_fare" class="ml-2 form-control" type="number"><br>
                                </div>
                                </div>`,
        preConfirm: () => {
            return new Promise((resolve, reject) => {
                let location = $('#add_rental_location').text().split('-')[0].trim(),
                car = $('#add_rental_car').text().split('-')[0].trim(),
                fare = $('#add_rental_fare').val()
                if (location.trim() == 'Location')
                    reject('Please select a location')
                else if (car.trim() == 'Car')
                    reject('Please select a car')
                else {
                    $.post('AddServiceServlet', { type: 'rental', location, car, fare}, data => {
                        if (data[0] == 'SUCCESS') {
                            resolve()
                        } else {
                            reject(data[1])
                        }
                    })
                }
            })
        }
    }).then(() => {loadCarRentalsIntoDiv()}).catch(() => {})
    }
    $.post('GetAllCarsServlet', cars => {
        if (!cars.length) {
            return false;
        }
        $.post('GetAllLocationsServlet', locs => {
            if (!locs.length) {
                return false;
            }
            makeAjaxCall(locs, cars)
        })
    })
})

// DELETE CAR POOLING SERVICE
$(document).on('click', '.delete_pooling', function() {
    if (!confirm('Are you sure you want to delete this service?'))
        return false
    $.post('DeleteServiceServlet', { pooling_id: this.id }, data => {
        if (data[0] != 'SUCCESS') {
            showErrorSwal('', data[1])
            return false
        }
        loadCarPoolsIntoDiv()
    })
})

// ADD CAR POOLING SERVICE
$(document).on('click', '#add_pooling', function() {
let makeAjaxCall = (locArr, carArr) => {
                    swal({
                                width: '70%',
                                height: '80%',
                                title: '<h2>Add Car Pooling Service</h2>',
                                html: `<div class="form-group" style="text-align: left">
                                <div class="dropdown">
                                    <a id="add_pooling_origin" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#" role="button">Origin<i class="caret"></i></a>
                                    <div class="dropdown-menu">
                                        ${locArr.map(loc => `<a class="dropdown-item" href="#">${loc.locationId} - ${loc.state}, ${loc.city}, ${loc.district}, ${loc.town}, ${loc.locality}</a>`).join("")}
                                    </div>
                                </div><br>
                                <div class="dropdown">
                                    <a id="add_pooling_destination" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#" role="button">Destination<i class="caret"></i></a>
                                    <div class="dropdown-menu">
                                        ${locArr.map(loc => `<a class="dropdown-item" href="#">${loc.locationId} - ${loc.state}, ${loc.city}, ${loc.district}, ${loc.town}, ${loc.locality}</a>`).join("")}
                                    </div>
                                </div><br>
                                <div class="dropdown">
                                    <a id="add_pooling_car" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#" role="button">Car</a>
                                    <div class="dropdown-menu">
                                        ${carArr.map(car => `<a class="dropdown-item" href="#">${car.carNumber} - ${car.carName}</a>`).join("")}
                                    </div>
                                </div><br>
                                <div class="form-inline">
                                Fare:
                                <input id="add_pooling_fare" class="ml-2 form-control" type="number"><br>
                                </div><br>
                                <div class="form-inline">
                                Departure date:
                                <input id="add_pooling_depdate" class="ml-2 form-control" type="text" placeholder="dd-mm-yyyy"><br>
                                </div><br>
                                <div class="form-inline">
                                Departure time:
                                <input id="add_pooling_deptime" class="ml-2 form-control" type="text" placeholder="hh:mm am/pm"><br>
                                </div>
                                </div>`,
        preConfirm: () => {
            return new Promise((resolve, reject) => {
                let origin = $('#add_pooling_origin').text().split('-')[0].trim(),
                destination = $('#add_pooling_destination').text().split('-')[0].trim(),
                car = $('#add_pooling_car').text().split('-')[0].trim(),
                fare = $('#add_pooling_fare').val(),
                depdate = $('#add_pooling_depdate').val(),
                deptime = $('#add_pooling_deptime').val()
                if (origin.trim() == 'Origin')
                    reject('Please select an origin')
                else if (destination.trim() == 'Destination')
                    reject('Please select a destination')
                else if (car.trim() == 'Car')
                    reject('Please select a car')
                else {
                    $.post('AddServiceServlet', { type: 'pooling', origin, destination, car, fare, depdate, deptime}, data => {
                        if (data[0] == 'SUCCESS') {
                            resolve()
                        } else {
                            reject(data[1])
                        }
                    })
                }
            })
        }
    }).then(() => {loadCarPoolsIntoDiv()}).catch(() => {})
    }
    $.post('GetAllCarsServlet', cars => {
        if (!cars.length) {
            return false;
        }
        $.post('GetAllLocationsServlet', locs => {
            if (!locs.length) {
                return false;
            }
            makeAjaxCall(locs, cars)
        })
    })
})

// DELETE LOCATION
$(document).on('click', '.delete_location', function() {
    if (!confirm('Are you sure you want to delete this location?'))
        return false
    $.post('DeleteLocationServlet', { locationId: this.id }, data => {
        if (data[0] != 'SUCCESS') {
            showErrorSwal('', data[1])
            return false
        }
        loadLocationsIntoDiv()
    })
})

// ADD LOCATION
$(document).on('click', '#add_location', function() {
    swal({
        title: '<h2>Add New Location</h2>',
        html: `
        <form class="form-group">
        <p>State:</p>
        <input id="state_input" class="form-control" type="text"><br>
        <p>City:</p>
        <input id="city_input" class="form-control" type="text"><br>
        <p>District:</p>
        <input id="district_input" class="form-control" type="text"><br>
        <p>Town:</p>
        <input id="town_input" class="form-control" type="text"><br>
        <p>Locality:</p>
        <input id="locality_input" class="form-control" type="text"><br>
        </form>
`,
        preConfirm: () => new Promise((resolve, reject) => {
            $.post('AddLocationServlet', {
                state: $('#state_input').val(),
                district: $('#district_input').val(),
                city: $('#city_input').val(),
                town: $('#town_input').val(),
                locality: $('#locality_input').val()
            }, data => {
                // alert(data)
                if (data[0] == 'SUCCESS')
                    resolve()
                else
                    showErrorSwal('', data[1])
            })
        }),
    }).then(() => {
        showSuccessSwal('', 'New location has been added successfully!')
        loadLocationsIntoDiv()
    }).catch(() => {})
})


//MODIFY ORDER
$(document).on('click', '#modifyorder', function() {})

// DELETE ORDER
$(document).on('click', '#deleteorder', function() {
    $.post('', { carnum: this.id }, data => {
        if (data[0] != 'SUCCESS') {
            showErrorSwal('', data[1])
            return false
        }
        loadCarsIntoDiv()
    })
})
