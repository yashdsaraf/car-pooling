package com.tyw.dao;

import com.tyw.bean.CustomerBean;
import com.tyw.bean.UserBean;
import com.tyw.enums.Error;
import com.tyw.enums.Role;

public interface UserDAO {

	public static final String TABLE = "user_details";

	public Error updatePasswd(String email, String passwd);

	public Error login(UserBean user, Role role);

	public Error register(CustomerBean customer);

}
