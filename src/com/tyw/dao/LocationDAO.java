package com.tyw.dao;

import com.google.gson.JsonArray;
import com.tyw.bean.LocationBean;
import com.tyw.enums.Error;

public interface LocationDAO {

	public static final String TABLE = "location";

	public LocationBean getLocationFromId(String locationId);

	public Error addLocation(LocationBean location);

	public Error deleteLocation(String locationId);

	public JsonArray getAllLocations();

}
