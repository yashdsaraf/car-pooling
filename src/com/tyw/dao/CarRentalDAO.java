package com.tyw.dao;

import java.util.Map;

import com.google.gson.JsonArray;
import com.tyw.bean.CarRentalBean;
import com.tyw.enums.Error;

public interface CarRentalDAO {

	public static final String TABLE = "carrental";
	public static final String VIEW = "rent_details";

	public JsonArray getFilterVals();

	public JsonArray searchCars(Map<String, String> params);

	public JsonArray getOrderDetails(String id, String uid);

	public Error deleteCarRental(String rentalId);

	public Error addService(CarRentalBean rental);

}
