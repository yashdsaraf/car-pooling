package com.tyw.dao;

import com.google.gson.JsonArray;
import com.tyw.bean.CarBean;
import com.tyw.enums.Error;

public interface CarDAO {

	public static final String TABLE = "car";

	public CarBean getCarFromNumber(String carNumber);

	public Error addCar(CarBean car);

	public Error deleteCar(String carNumber);

	public JsonArray getAllCars();

	public Error modifyCar(String carNumber, int passnum);

}
