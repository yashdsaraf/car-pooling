package com.tyw.dao;

import com.google.gson.JsonArray;
import com.tyw.bean.BookingBean;
import com.tyw.bean.CustomerBean;
import com.tyw.enums.Error;

public interface CustomerDAO {

	public static final String TABLE = "customer";
	public static final String VIEW = "cus_details";

	public boolean checkIfExists(String col, String val);

	public Error register(CustomerBean customer);

	public Error modifyOrder(BookingBean order);

	public Error cancelOrder(String orderId);

	public Error modifyProfile(CustomerBean customer);

	public Error deleteProfile(CustomerBean customer);

	public CustomerBean getCustomerFromId(String id);

	public Error placeOrder(String serviceId, JsonArray orderDetails);

}
