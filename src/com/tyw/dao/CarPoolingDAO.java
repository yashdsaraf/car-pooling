package com.tyw.dao;

import java.util.Map;

import com.google.gson.JsonArray;
import com.tyw.bean.CarPoolingBean;
import com.tyw.enums.Error;

public interface CarPoolingDAO {

	public static final String TABLE = "carpooling";
	public static final String VIEW = "pool_details";

	public JsonArray getFilterVals();

	public JsonArray searchCars(Map<String, String> params);

	public JsonArray getOrderDetails(String id, String uid);

	public Error deleteCarPooling(String poolingId);

	public Error addService(CarPoolingBean pooling);

}
