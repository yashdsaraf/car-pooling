package com.tyw.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CarRentalBean implements Serializable {

	private String rentalId;
	private String carNumber;
	private double fare;
	private String locationId;
	private CarBean car;
	private LocationBean location;

	public LocationBean getLocation() {
		return location;
	}

	public void setLocation(LocationBean location) {
		this.location = location;
	}

	public String getRentalId() {
		return rentalId;
	}

	public void setRentalId(String rentalId) {
		this.rentalId = rentalId;
	}

	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	public double getFare() {
		return fare;
	}

	public void setFare(double fare) {
		this.fare = fare;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public CarBean getCar() {
		return car;
	}

	public void setCar(CarBean car) {
		this.car = car;
	}

}