package com.tyw.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CustomerBean extends UserBean implements Serializable {

	private String id;
	private long phone;
	private String email;
	private double balance;
	private String licenseNo;
	private String postalAddress;
	private String locationId;

	public CustomerBean() {

	}

	public CustomerBean(String id, long phone, String email, double balance, String licenseNo, String postalAddress,
			String locationId) {
		super();
		this.id = id;
		this.phone = phone;
		this.email = email;
		this.balance = balance;
		this.licenseNo = licenseNo;
		this.postalAddress = postalAddress;
		this.locationId = locationId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getLicenseNo() {
		return licenseNo;
	}

	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}

	public String getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

}
