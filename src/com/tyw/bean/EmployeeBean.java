package com.tyw.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class EmployeeBean extends UserBean implements Serializable {

	private String id;
	private String qualification;
	private String locationId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

}
