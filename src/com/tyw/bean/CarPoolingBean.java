package com.tyw.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CarPoolingBean implements Serializable {

	private String poolingId;
	private String carNumber;
	private double fare;
	private String originId;
	private String destinationId;
	private String departure;
	private CarBean car;
	private LocationBean origin;
	private LocationBean destination;

	public String getOriginId() {
		return originId;
	}

	public void setOriginId(String originId) {
		this.originId = originId;
	}

	public String getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}

	public LocationBean getOrigin() {
		return origin;
	}

	public void setOrigin(LocationBean origin) {
		this.origin = origin;
	}

	public LocationBean getDestination() {
		return destination;
	}

	public void setDestination(LocationBean destination) {
		this.destination = destination;
	}

	public String getPoolingId() {
		return poolingId;
	}

	public void setPoolingId(String poolingId) {
		this.poolingId = poolingId;
	}

	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	public double getFare() {
		return fare;
	}

	public void setFare(double fare) {
		this.fare = fare;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public CarBean getCar() {
		return car;
	}

	public void setCar(CarBean car) {
		this.car = car;
	}

}