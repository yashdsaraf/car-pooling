package com.tyw.bean;

import java.io.Serializable;

import com.tyw.enums.BookingStatus;

@SuppressWarnings("serial")
public class BookingBean implements Serializable {

	private String orderNo;
	private String customerId;
	private String carNumber;
	private String serviceId;
	private String bookingDate;
	private String endDate;
	private double fare;
	private double tax;
	private double taxRate;
	private int noOfPassengers;
	private String pickupPoint;
	private String dropoffPoint;
	private BookingStatus status;
	private String cancellationReason;
	private CustomerBean customer;
	private String orderType;

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public CustomerBean getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerBean customer) {
		this.customer = customer;
	}

	public BookingBean() {
	}

	public BookingBean(String orderNo, String customerId, String carNumber, String serviceId, String bookingDate,
			String endDate, double fare, double tax, double taxRate, int noOfPassengers, String pickupPoint,
			String dropoffPoint, BookingStatus status, String cancellationReason) {
		super();
		this.orderNo = orderNo;
		this.customerId = customerId;
		this.carNumber = carNumber;
		this.serviceId = serviceId;
		this.bookingDate = bookingDate;
		this.endDate = endDate;
		this.fare = fare;
		this.tax = tax;
		this.taxRate = taxRate;
		this.noOfPassengers = noOfPassengers;
		this.pickupPoint = pickupPoint;
		this.dropoffPoint = dropoffPoint;
		this.status = status;
		this.cancellationReason = cancellationReason;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public double getFare() {
		return fare;
	}

	public void setFare(double fare) {
		this.fare = fare;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public int getNoOfPassengers() {
		return noOfPassengers;
	}

	public void setNoOfPassengers(int noOfPassengers) {
		this.noOfPassengers = noOfPassengers;
	}

	public String getPickupPoint() {
		return pickupPoint;
	}

	public void setPickupPoint(String pickupPoint) {
		this.pickupPoint = pickupPoint;
	}

	public String getDropoffPoint() {
		return dropoffPoint;
	}

	public void setDropoffPoint(String dropoffPoint) {
		this.dropoffPoint = dropoffPoint;
	}

	public BookingStatus getStatus() {
		return status;
	}

	public void setStatus(BookingStatus status) {
		this.status = status;
	}

	public String getCancellationReason() {
		return cancellationReason;
	}

	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}

}