package com.tyw.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tyw.bean.CarBean;
import com.tyw.bean.CarPoolingBean;
import com.tyw.bean.CustomerBean;
import com.tyw.bean.LocationBean;
import com.tyw.dao.BookingDAO;
import com.tyw.dao.CarPoolingDAO;
import com.tyw.dao.CustomerDAO;
import com.tyw.dao.LocationDAO;
import com.tyw.enums.Error;
import com.tyw.util.DBConnection;

public class CarPoolingDAOImpl implements CarPoolingDAO {

	private Connection conn;

	@Override
	public JsonArray getFilterVals() {
		try {
			conn = DBConnection.getConnection();
			String query = "select * from " + VIEW;
			JsonArray json = new JsonArray();
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				ResultSet rs = stmt.executeQuery();
				if (!rs.next()) {
					// return null;
				}
				int total = rs.getMetaData().getColumnCount();
				do {
					JsonObject jobj = new JsonObject();
					for (int i = 1; i <= total; i++) {
						jobj.addProperty(rs.getMetaData().getColumnLabel(i).toLowerCase(), rs.getString(i));
					}
					json.add(jobj);
				} while (rs.next());
			}
			return json;
		} catch (Exception e) {

		}
		JsonArray array = new JsonArray();
		array.add(true);
		return array;
	}
	
	@Override
	public Error addService(CarPoolingBean pooling) {
		Error toReturn = Error.DATABASE;
		try {
			conn = DBConnection.getConnection();
			String query = "insert into " + TABLE + " values (getUniqueVal(),?,?,?,?,TO_DATE(?, 'DD-MM-YYYY HH:MI AM'))";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, pooling.getCarNumber());
				stmt.setDouble(2, pooling.getFare());
				stmt.setString(3, pooling.getOriginId());
				stmt.setString(4, pooling.getDestinationId());
				stmt.setString(5, pooling.getDeparture());
				if (stmt.executeUpdate() > 0) {
					toReturn = Error.SUCCESS;
				}
			}
		} catch (SQLException e) {
			toReturn.setDescription(e.getMessage());
			return toReturn;
		}
		return toReturn;
	}
	
	@Override
	public Error deleteCarPooling(String poolingId) {
		Error toReturn = Error.DATABASE;
		try {
			conn = DBConnection.getConnection();
			isServiceBooked(poolingId);
			String query = "delete from " + TABLE + " where pooling_id=?";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, poolingId);
				stmt.executeUpdate();
				toReturn = Error.SUCCESS;
			}
		} catch (SQLException e) {
			toReturn.setDescription(e.getMessage());
			return toReturn;
		}
		return toReturn;
	}

	@Override
	public JsonArray searchCars(Map<String, String> params) {
		StringBuffer query = new StringBuffer(
				"select fare, pooling_id, origin, destination, car_number, deptime, depdate from " + VIEW);
		JsonArray toReturn = new JsonArray();
		Iterator<String> iter = params.keySet().iterator();
		String param, value;
		String[] valArr;
		boolean first = true;
		while (iter.hasNext()) {
			param = iter.next();
			value = params.get(param).toLowerCase();
			if (first) {
				query.append(" where ");
				first = false;
			} else {
				query.append(" and ");
			}
			switch (param) {
			case "name":
				query.append(" car_name=\'" + value + "\'");
				break;
			case "type":
				query.append(" car_type=\'" + value + "\'");
				break;
			case "passnum":
				query.append(" no_of_passengers>=" + value);
				query.append(" and " + value + ">0");
				break;
			case "origin":
				valArr = value.split(",");
				query.append(" LOWER(otown)=\'" + valArr[0].trim() + "\'");
				query.append(" and LOWER(ocity)=\'" + valArr[1].trim() + "\'");
				query.append(" and LOWER(ostate)=\'" + valArr[2].trim() + "\'");
				break;
			case "destination":
				valArr = value.split(",");
				query.append(" LOWER(town)=\'" + valArr[0].trim() + "\'");
				query.append(" and LOWER(city)=\'" + valArr[1].trim() + "\'");
				query.append(" and LOWER(state)=\'" + valArr[2].trim() + "\'");
				break;
			case "deptime":
				query.append(" LOWER(deptime)=\'" + value + "\'");
				break;
			case "depdate":
				query.append(" depdate=\'" + value + "\'");
				break;
			default:
				return null;
			}
		}
		try {
			conn = DBConnection.getConnection();
			try (PreparedStatement stmt = conn.prepareStatement(query.toString())) {
				ResultSet rs = stmt.executeQuery();
				if (!rs.next()) {
					throw new SQLException("No results found");
				}
				do {
					CarPoolingBean bean = new CarPoolingBean();
					bean.setPoolingId(rs.getString("pooling_id"));
					bean.setFare(rs.getDouble("fare"));
					bean.setOriginId(rs.getString("origin"));
					bean.setDestinationId(rs.getString("destination"));
					bean.setCarNumber(rs.getString("car_number"));
					bean.setDeparture(rs.getString("depdate") + " " + rs.getString("deptime"));
					LocationDAO location = new LocationDAOImpl();
					LocationBean origin = location.getLocationFromId(bean.getOriginId());
					LocationBean destination = location.getLocationFromId(bean.getDestinationId());
					CarBean car = new CarDAOImpl().getCarFromNumber(bean.getCarNumber());
					bean.setOrigin(origin);
					bean.setDestination(destination);
					bean.setCar(car);
					toReturn.add(new JsonParser().parse(new Gson().toJson(bean)));
				} while (rs.next());
				return toReturn;
			}
		} catch (SQLException e) {
			return null;
		}
	}

	@Override
	public JsonArray getOrderDetails(String id, String uid) {
		try {
			conn = DBConnection.getConnection();
			String query = "select * from " + VIEW + " where pooling_id=?";
			JsonArray jArray = new JsonArray();
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, id);
				ResultSet rs = stmt.executeQuery();
				if (!rs.next()) {
					return null;
				}
				JsonObject jObj = new JsonObject();
				CustomerDAO customer = new CustomerDAOImpl();
				CustomerBean customerBean = customer.getCustomerFromId(uid);
				jObj.addProperty("email", customerBean.getEmail());
				jObj.addProperty("phone", customerBean.getPhone());
				jObj.addProperty("customerid", customerBean.getId());
				jObj.addProperty("carname", rs.getString("car_name"));
				jObj.addProperty("cartype", rs.getString("car_type"));
				jObj.addProperty("carnum", rs.getString("car_number"));
				jObj.addProperty("passnum", rs.getString("no_of_passengers"));
				jObj.addProperty("fare", rs.getString("fare"));
				jObj.addProperty("origin", rs.getString("ostate") + ", " + rs.getString("ocity") + ", "
						+ rs.getString("odistrict") + ", " + rs.getString("otown") + ", " + rs.getString("olocality"));
				jObj.addProperty("destination", rs.getString("state") + ", " + rs.getString("city") + ", "
						+ rs.getString("district") + ", " + rs.getString("town") + ", " + rs.getString("locality"));
				jObj.addProperty("originid", rs.getString("origin"));
				jObj.addProperty("destinationid", rs.getString("destination"));
				jObj.addProperty("departure", rs.getString("depdate") + " " + rs.getString("deptime"));
				jArray.add(jObj);
				return jArray;
			}
		} catch (SQLException e) {
			return new JsonArray();
		}
	}
	
	private void isServiceBooked(String poolingId) throws SQLException {
		String query = "select * from " + BookingDAO.TABLE
				+ " where service_id=? and status not in ('CAN','COMP')";
		try (PreparedStatement stmt = conn.prepareStatement(query)) {
			stmt.setString(1, poolingId);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				throw new SQLException("A booked service cannot be deleted");
			}
		}

	}

}
