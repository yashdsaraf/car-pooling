package com.tyw.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tyw.bean.CustomerBean;
import com.tyw.bean.UserBean;
import com.tyw.dao.UserDAO;
import com.tyw.enums.Error;
import com.tyw.enums.Role;
import com.tyw.util.DBConnection;

public class UserDAOImpl implements UserDAO {

	private Connection conn = null;
	private static final String INSERTUSERDETAILS = "insert into " + TABLE + " values (?,?,INITCAP(?),INITCAP(?),?)";

	@Override
	public Error register(CustomerBean customer) {
		try {
			conn = DBConnection.getConnection();
			try (PreparedStatement stmt = conn.prepareStatement(INSERTUSERDETAILS)) {
				stmt.setString(1, customer.getUserId());
				stmt.setString(2, customer.getLoginId());
				stmt.setString(3, customer.getFname());
				stmt.setString(4, customer.getLname());
				stmt.setString(5, customer.getPasswd());
				if (stmt.executeUpdate() == 0) {
					throw new SQLException("Error while inserting user details");
				}
			}
		} catch (SQLException e) {
			return Error.DATABASE;
		}
		return Error.SUCCESS;
	}

	@Override
	public Error login(UserBean user, Role role) {
		try {
			conn = DBConnection.getConnection();
			ResultSet rs;
			String query = "select user_id,fname,lname,passwd from " + TABLE + " where login_id=?";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, user.getLoginId());
				rs = stmt.executeQuery();
				if (!rs.next()) {
					return Error.USERNOTEXIST;
				}
			}
			query = "select * from " + role.getTable() + " u join " + TABLE
					+ " d on u.user_id=d.user_id where login_id=?";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, user.getLoginId());
				rs = stmt.executeQuery();
				if (!rs.next()) {
					return Error.WRONGROLE;
				}
				if (!user.getPasswd().equals(rs.getString("passwd"))) {
					return Error.INCORRECTPASSWORD;
				}
				user.setFname(rs.getString("fname"));
				user.setLname(rs.getString("lname"));
				user.setUserId(rs.getString("user_id"));
				// if (role == Role.EMPLOYEE) {
				// user.setQualification(rs.getString("qualification"));
				// }
			}
		} catch (SQLException e) {
			return Error.DATABASE;
		}

		return Error.SUCCESS;
	}

	@Override
	public Error updatePasswd(String email, String passwd) {
		try {
			conn = DBConnection.getConnection();
			String query = "update " + TABLE + " set passwd=? where user_id=(" + "select user_id from ("
					+ "        select user_id, email from " + Role.EMPLOYEE.getTable() + "        union"
					+ "        select user_id, email from " + Role.CUSTOMER.getTable() + "    ) where email=?)";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, passwd);
				stmt.setString(2, email);
				if (stmt.executeUpdate() == 0) {
					return Error.DATABASE;
				}
			}
		} catch (SQLException e) {
			return Error.DATABASE;
		}
		return Error.SUCCESS;
	}

}
