package com.tyw.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.tyw.bean.CarBean;
import com.tyw.dao.CarDAO;
import com.tyw.enums.Error;
import com.tyw.util.DBConnection;

public class CarDAOImpl implements CarDAO {

	private Connection conn;

	@Override
	public CarBean getCarFromNumber(String carNumber) {
		try {
			conn = DBConnection.getConnection();
			String query = "select * from " + TABLE + " where car_number=?";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, carNumber);
				ResultSet rs = stmt.executeQuery();
				if (!rs.next()) {
					throw new SQLException("No results found");
				}
				CarBean bean = new CarBean();
				bean.setAvailability(rs.getString("availability"));
				bean.setCarImage(rs.getString("car_image"));
				bean.setCarName(rs.getString("car_name"));
				bean.setCarNumber(rs.getString("car_number"));
				bean.setCarType(rs.getString("car_type"));
				bean.setNoOfPassengers(rs.getInt("no_of_passengers"));
				bean.setOrderType(rs.getString("order_type"));
				return bean;
			}
		} catch (SQLException e) {
			return null;
		}
	}

	@Override
	public Error addCar(CarBean car) {
		Error toReturn = Error.DATABASE;
		try {
			conn = DBConnection.getConnection();
			String query = "insert into " + TABLE + " values (?,?,?,?,'AVAILABLE',?,?)";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, car.getCarNumber());
				stmt.setString(2, car.getCarName());
				stmt.setString(3, car.getCarType());
				stmt.setString(4, car.getOrderType());
				stmt.setInt(5, car.getNoOfPassengers());
				stmt.setString(6, car.getCarImage());
				if (stmt.executeUpdate() > 0) {
					toReturn = Error.SUCCESS;
				}
			}
		} catch (SQLException e) {
			toReturn.setDescription(e.getMessage());
			return toReturn;
		}
		return toReturn;
	}

	@Override
	public Error deleteCar(String carNumber) {
		Error toReturn = Error.DATABASE;
		try {
			conn = DBConnection.getConnection();
			getAvailability(conn, carNumber);
			String query = "delete from " + TABLE + " where car_number=?";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, carNumber);
				stmt.executeUpdate();
				toReturn = Error.SUCCESS;
			}
		} catch (SQLException e) {
			toReturn.setDescription(e.getMessage());
			return toReturn;
		}
		return toReturn;
	}

	@Override
	public JsonArray getAllCars() {
		JsonArray toReturn = new JsonArray();
		try {
			conn = DBConnection.getConnection();
			String query = "select * from " + TABLE;
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				ResultSet rs = stmt.executeQuery();
				if (!rs.next()) {
					return null;
				}
				do {
					CarBean bean = new CarBean();
					bean.setCarNumber(rs.getString("car_number"));
					bean.setCarName(rs.getString("car_name"));
					bean.setCarType(rs.getString("car_type"));
					bean.setOrderType(rs.getString("order_type"));
					bean.setAvailability(rs.getString("availability"));
					bean.setNoOfPassengers(rs.getInt("no_of_passengers"));
					bean.setCarImage(rs.getString("car_image"));
					toReturn.add(new JsonParser().parse(new Gson().toJson(bean)));
				} while (rs.next());
			}
		} catch (SQLException e) {
			return null;
		}
		return toReturn;
	}

	@Override
	public Error modifyCar(String carNumber, int passnum) {
		Error toReturn = Error.DATABASE;
		try {
			conn = DBConnection.getConnection();
			getAvailability(conn, carNumber);
			String query = "update " + TABLE + " set no_of_passengers=? where car_number=?";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setInt(1, passnum);
				stmt.setString(2, carNumber);
				stmt.executeUpdate();
				toReturn = Error.SUCCESS;
			}
		} catch (SQLException e) {
			toReturn.setDescription(e.getMessage());
			return toReturn;
		}
		return toReturn;
	}

	private void getAvailability(Connection conn, String carNumber) throws SQLException {
		String query = "select availability from " + TABLE + " where car_number=?";
		try (PreparedStatement stmt = conn.prepareStatement(query)) {
			stmt.setString(1, carNumber);
			ResultSet rs = stmt.executeQuery();
			if (!rs.next()) {
				throw new SQLException("Car not found");
			}
			if (!rs.getString("availability").equals("AVAILABLE")) {
				throw new SQLException("A booked car cannot be modified or deleted");
			}
		}

	}
}