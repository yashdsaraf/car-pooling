package com.tyw.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tyw.bean.BookingBean;
import com.tyw.bean.CustomerBean;
import com.tyw.dao.BookingDAO;
import com.tyw.dao.CarDAO;
import com.tyw.dao.CustomerDAO;
import com.tyw.enums.Error;
import com.tyw.util.DBConnection;

public class CustomerDAOImpl implements CustomerDAO {

	private static final String INSERTCUSTOMER = "insert into " + TABLE
			+ " (customer_id, user_id, phone, email, license, address) values(?,?,?,LOWER(?),UPPER(?),?)";
	private Connection conn = null;

	@Override
	public CustomerBean getCustomerFromId(String id) {
		try {
			conn = DBConnection.getConnection();
			String query = "select * from " + VIEW + " where user_id=?";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, id);
				ResultSet rs = stmt.executeQuery();
				if (!rs.next()) {
					throw new SQLException("No results found");
				}
				CustomerBean bean = new CustomerBean(rs.getString("customer_id"), rs.getLong("phone"),
						rs.getString("email"), rs.getDouble("balance"), rs.getString("license"),
						rs.getString("address"), rs.getString("location_id"));
				return bean;
			}
		} catch (SQLException e) {
			return null;
		}
	}

	@Override
	public boolean checkIfExists(String col, String val) {
		String query = "select * from " + VIEW + " where " + col + "=?";
		try {
			conn = DBConnection.getConnection();
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, val);
				if (!stmt.executeQuery().next()) {
					return false;
				}
			}
		} catch (SQLException e) {
			return false;
		}
		return true;
	}

	@Override
	public Error register(CustomerBean customer) {
		try {
			conn = DBConnection.getConnection();
			customer.setUserId(DBConnection.getUniqueString());
			customer.setId(DBConnection.getUniqueString());
			new UserDAOImpl().register(customer);
			try (PreparedStatement stmt = conn.prepareStatement(INSERTCUSTOMER)) {
				stmt.setString(1, customer.getId());
				stmt.setString(2, customer.getUserId());
				stmt.setLong(3, customer.getPhone());
				stmt.setString(4, customer.getEmail());
				stmt.setString(5, customer.getLicenseNo());
				stmt.setString(6, customer.getPostalAddress());
				if (stmt.executeUpdate() == 0) {
					throw new SQLException("Error while inserting customer details");
				}
			}
		} catch (SQLException e) {
			return Error.DATABASE;
		}
		return Error.SUCCESS;
	}

	// public String login1(String loginId, String passwd) {
	// try {
	// conn = DBConnection.getConnection();
	// try (Statement stmt = conn.createStatement()) {
	// String query = "select passwd from customer where login_id=\'" + loginId
	// +
	// "\'";
	// if (stmt.execute(query)) {
	// ResultSet rs = stmt.getResultSet();
	// rs.next();
	// return String.valueOf(passwd.equals(rs.getString(1)));
	// }
	// }
	// } catch (SQLException e) {
	// return e.getMessage();
	// }
	// return null;
	// }

	@Override
	public Error placeOrder(String serviceId, JsonArray orderDetails) {
		JsonObject jObj = orderDetails.get(0).getAsJsonObject();
		try {
			conn = DBConnection.getConnection();
			String query = "insert into " + BookingDAO.TABLE
					+ " values (getUniqueVal(),?,?,?,?,TO_DATE(?, 'DD-MM-YYYY HH:MI AM'),NULL,?,NULL,NULL,?,?,?,'PEN',NULL)";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, jObj.get("customerid").getAsString());
				stmt.setString(2, jObj.get("carnum").getAsString());
				stmt.setString(3, serviceId);
				stmt.setDouble(6, jObj.get("fare").getAsDouble());
				stmt.setInt(7, jObj.get("passnum").getAsInt());
				if (jObj.has("location")) {
					LocalDateTime localDate = LocalDateTime.now();
					stmt.setString(5, DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm a").format(localDate));
					stmt.setString(4, "RENT");
					stmt.setString(8, jObj.get("location").getAsString());
					stmt.setString(9, jObj.get("location").getAsString());
				} else {
					stmt.setString(4, "POOL");
					stmt.setString(5, jObj.get("departure").getAsString());
					stmt.setString(8, jObj.get("originid").getAsString());
					stmt.setString(9, jObj.get("destinationid").getAsString());
				}
				if (stmt.executeUpdate() == 0) {
					return Error.DATABASE;
				}
			}
			query = "update " + CarDAO.TABLE + " set availability='BOOKED' where car_number=?";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, jObj.get("carnum").getAsString());
				if (stmt.executeUpdate() == 0) {
					return Error.DATABASE;
				}
			}
			return Error.SUCCESS;
		} catch (SQLException e) {
			Error result = Error.DATABASE;
			result.setDescription(e.getMessage());
			return result;
		}

	}

	@Override
	public Error modifyOrder(BookingBean order) {

		return null;
	}

	@Override
	public Error modifyProfile(CustomerBean customer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Error deleteProfile(CustomerBean customer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Error cancelOrder(String orderNo) {
		Error toReturn = Error.DATABASE;
		try {
			conn = DBConnection.getConnection();
			String query = "update " + BookingDAO.TABLE + " set status='PENCAN' where order_no=?";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, orderNo);
				if (stmt.executeUpdate() > 0) {
					toReturn = Error.SUCCESS;
				}
			}
		} catch (SQLException e) {
			toReturn.setDescription(e.getMessage());
			return toReturn;
		}
		return toReturn;
	}

}
