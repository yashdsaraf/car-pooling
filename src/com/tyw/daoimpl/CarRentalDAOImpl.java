package com.tyw.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tyw.bean.CarBean;
import com.tyw.bean.CarRentalBean;
import com.tyw.bean.CustomerBean;
import com.tyw.bean.LocationBean;
import com.tyw.dao.BookingDAO;
import com.tyw.dao.CarRentalDAO;
import com.tyw.dao.CustomerDAO;
import com.tyw.enums.Error;
import com.tyw.util.DBConnection;

public class CarRentalDAOImpl implements CarRentalDAO {

	private Connection conn;

	@Override
	public JsonArray getFilterVals() {
		try {
			conn = DBConnection.getConnection();
			String query = "select * from " + VIEW;
			JsonArray json = new JsonArray();
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				ResultSet rs = stmt.executeQuery();
				if (!rs.next()) {
					throw new SQLException("No results found");
				}
				int total = rs.getMetaData().getColumnCount();
				do {
					JsonObject jobj = new JsonObject();
					for (int i = 1; i <= total; i++) {
						jobj.addProperty(rs.getMetaData().getColumnLabel(i).toLowerCase(), rs.getString(i));
					}
					json.add(jobj);
				} while (rs.next());
			}
			return json;
		} catch (SQLException e) {
			return null;
		}
	}

	@Override
	public Error deleteCarRental(String rentalId) {
		Error toReturn = Error.DATABASE;
		try {
			conn = DBConnection.getConnection();
			isServiceBooked(rentalId);
			String query = "delete from " + TABLE + " where rental_id=?";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, rentalId);
				stmt.executeUpdate();
				toReturn = Error.SUCCESS;
			}
		} catch (SQLException e) {
			toReturn.setDescription(e.getMessage());
			return toReturn;
		}
		return toReturn;
	}

	
	@Override
	public JsonArray searchCars(Map<String, String> params) {
		StringBuffer query = new StringBuffer(
				"select fare, rental_id, location_id, car_number from " + VIEW);
		JsonArray toReturn = new JsonArray();
		Iterator<String> iter = params.keySet().iterator();
		String param, value;
		boolean first = true;
		while (iter.hasNext()) {
			if (first) {
				query.append(" where ");
				first = false;
			} else {
				query.append(" and ");
			}
			param = iter.next();
			value = params.get(param).toLowerCase();
			switch (param) {
			case "name":
				query.append(" LOWER(car_name)=\'" + value + "\'");
				break;
			case "type":
				query.append(" LOWER(car_type)=\'" + value + "\'");
				break;
			case "passnum":
				query.append(" no_of_passengers>=" + value);
				query.append(" and " + value + ">0");
				break;
			case "location":
				String[] valArr = value.split(",");
				query.append(" LOWER(town)=\'" + valArr[0].trim() + "\'");
				query.append(" and LOWER(city)=\'" + valArr[1].trim() + "\'");
				query.append(" and LOWER(state)=\'" + valArr[2].trim() + "\'");
				break;
			default:
				return null;
			}
		}
		try {
			conn = DBConnection.getConnection();
			try (PreparedStatement stmt = conn.prepareStatement(query.toString())) {
				ResultSet rs = stmt.executeQuery();
				if (!rs.next()) {
					throw new SQLException("No results found");
				}
				do {
					CarRentalBean bean = new CarRentalBean();
					bean.setRentalId(rs.getString("rental_id"));
					bean.setLocationId(rs.getString("location_id"));
					bean.setCarNumber(rs.getString("car_number"));
					bean.setFare(rs.getDouble("fare"));
					LocationBean location = new LocationDAOImpl().getLocationFromId(bean.getLocationId());
					CarBean car = new CarDAOImpl().getCarFromNumber(bean.getCarNumber());
					bean.setLocation(location);
					bean.setCar(car);
					toReturn.add(new JsonParser().parse(new Gson().toJson(bean)));
				} while (rs.next());
				return toReturn;
			}
		} catch (SQLException e) {
			return null;
		}
	}

	@Override
	public JsonArray getOrderDetails(String id, String uid) {
		try {
			conn = DBConnection.getConnection();
			String query = "select * from " + VIEW + " where rental_id=?";
			JsonArray jArray = new JsonArray();
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, id);
				ResultSet rs = stmt.executeQuery();
				if (!rs.next()) {
					return null;
				}
				JsonObject jObj = new JsonObject();
				CustomerDAO customer = new CustomerDAOImpl();
				CustomerBean customerBean = customer.getCustomerFromId(uid);
				jObj.addProperty("email", customerBean.getEmail());
				jObj.addProperty("phone", customerBean.getPhone());
				jObj.addProperty("customerid", customerBean.getId());
				jObj.addProperty("carname", rs.getString("car_name"));
				jObj.addProperty("cartype", rs.getString("car_type"));
				jObj.addProperty("carnum", rs.getString("car_number"));
				jObj.addProperty("passnum", rs.getString("no_of_passengers"));
				jObj.addProperty("fare", rs.getString("fare"));
				jObj.addProperty("location", rs.getString("location_id"));
				jArray.add(jObj);
				return jArray;
			}
		} catch (SQLException e) {
			return new JsonArray();
		}
	}
	
	@Override
	public Error addService(CarRentalBean rental) {
		Error toReturn = Error.DATABASE;
		try {
			conn = DBConnection.getConnection();
			String query = "insert into " + TABLE + " values (getUniqueVal(),?,?,?)";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, rental.getCarNumber());
				stmt.setDouble(2, rental.getFare());
				stmt.setString(3, rental.getLocationId());
				if (stmt.executeUpdate() > 0) {
					toReturn = Error.SUCCESS;
				}
			}
		} catch (SQLException e) {
			toReturn.setDescription(e.getMessage());
			return toReturn;
		}
		return toReturn;
	}

	
	private void isServiceBooked(String rentalId) throws SQLException {
		String query = "select * from " + BookingDAO.TABLE
				+ " where service_id=? and status not in ('CAN','COMP')";
		try (PreparedStatement stmt = conn.prepareStatement(query)) {
			stmt.setString(1, rentalId);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				throw new SQLException("A booked service cannot be deleted");
			}
		}

	}

}
