package com.tyw.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.tyw.bean.LocationBean;
import com.tyw.dao.BookingDAO;
import com.tyw.dao.LocationDAO;
import com.tyw.enums.Error;
import com.tyw.util.DBConnection;

public class LocationDAOImpl implements LocationDAO {

	private Connection conn;

	@Override
	public LocationBean getLocationFromId(String locationId) {
		try {
			conn = DBConnection.getConnection();
			String query = "select * from " + TABLE + " where location_id=?";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, locationId);
				ResultSet rs = stmt.executeQuery();
				if (!rs.next()) {
					throw new SQLException("No results found");
				}
				LocationBean bean = new LocationBean();
				bean.setLocationId(rs.getString("location_id"));
				bean.setState(rs.getString("state"));
				bean.setCity(rs.getString("city"));
				bean.setDistrict(rs.getString("district"));
				bean.setTown(rs.getString("town"));
				bean.setLocality(rs.getString("locality"));
				bean.setLatitude(rs.getString("latitude"));
				bean.setLongitude(rs.getString("longitude"));
				return bean;
			}
		} catch (SQLException e) {
			return null;
		}
	}

	@Override
	public Error addLocation(LocationBean location) {
		Error toReturn = Error.DATABASE;
		try {
			conn = DBConnection.getConnection();
			String query = "insert into " + TABLE + " values (getUniqueVal(),?,?,?,?,?,NULL,NULL)";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, location.getState());
				stmt.setString(2, location.getCity());
				stmt.setString(3, location.getDistrict());
				stmt.setString(4, location.getTown());
				stmt.setString(5, location.getLocality());
				if (stmt.executeUpdate() > 0) {
					toReturn = Error.SUCCESS;
				}
			}
		} catch (SQLException e) {
			toReturn.setDescription(e.getMessage());
			return toReturn;
		}
		return toReturn;
	}

	@Override
	public Error deleteLocation(String locationId) {
		Error toReturn = Error.DATABASE;
		try {
			conn = DBConnection.getConnection();
			isLocationBooked(locationId);
			String query = "delete from " + TABLE + " where location_id=?";
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, locationId);
				stmt.executeUpdate();
				toReturn = Error.SUCCESS;
			}
		} catch (SQLException e) {
			toReturn.setDescription(e.getMessage());
			return toReturn;
		}
		return toReturn;
	}

	@Override
	public JsonArray getAllLocations() {
		JsonArray toReturn = new JsonArray();
		try {
			conn = DBConnection.getConnection();
			String query = "select * from " + TABLE;
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				ResultSet rs = stmt.executeQuery();
				if (!rs.next()) {
					return null;
				}
				do {
					LocationBean bean = new LocationBean();
					bean.setLocationId(rs.getString("location_id"));
					bean.setState(rs.getString("state"));
					bean.setCity(rs.getString("city"));
					bean.setDistrict(rs.getString("district"));
					bean.setTown(rs.getString("town"));
					bean.setLocality(rs.getString("locality"));
					toReturn.add(new JsonParser().parse(new Gson().toJson(bean)));
				} while (rs.next());
			}
		} catch (SQLException e) {
			return null;
		}
		return toReturn;
	}

	private void isLocationBooked(String locationId) throws SQLException {
		String query = "select * from " + BookingDAO.TABLE
				+ " where (pickup_point=? or dropoff_point=?) and status not in ('CAN','COMP')";
		try (PreparedStatement stmt = conn.prepareStatement(query)) {
			stmt.setString(1, locationId);
			stmt.setString(2, locationId);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				throw new SQLException("A booked location cannot be deleted");
			}
		}

	}

}