package com.tyw.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.tyw.bean.BookingBean;
import com.tyw.bean.CarBean;
import com.tyw.bean.CarPoolingBean;
import com.tyw.bean.InvoiceBean;
import com.tyw.dao.EmployeeDAO;
import com.tyw.enums.BookingStatus;
import com.tyw.enums.Error;
import com.tyw.util.DBConnection;

public class EmployeeDAOImpl implements EmployeeDAO {

	private Connection conn;

	@Override
	public boolean checkIfExists(String col, String val) {
		String query = "select * from " + VIEW + " where " + col + "=?";
		try {
			conn = DBConnection.getConnection();
			try (PreparedStatement stmt = conn.prepareStatement(query)) {
				stmt.setString(1, val);
				if (!stmt.executeQuery().next()) {
					return false;
				}
			}
		} catch (SQLException e) {
			return false;
		}
		return true;
	}

	@Override
	public Error addCar(CarBean car) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Error deleteCar(CarBean car) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Error calculateFareAndTaxes(BookingBean order) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Error modifyOrder(BookingBean order) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Error cancelOrder(BookingBean order) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Error assignCar(BookingBean order, String carNum) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Error addCarPoolingService(CarPoolingBean service) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Error modifyNoOfPassengers(String carNum, int noOfPassengers) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Error editBookingRequest(BookingBean request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Error updateBookingStatus(String orderNum, BookingStatus status) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InvoiceBean generateCustomerInvoice(String orderNum) {
		// TODO Auto-generated method stub
		return null;
	}

}
