package com.tyw.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;
import javax.naming.NamingException;

public class SendEmail {
	private Session session;
	private String recipient, message, subject;
	private String email, password, host, port;

	public SendEmail(String recipient, String subject, String message) {
		this.recipient = recipient;
		this.subject = subject;
		this.message = message;
		ReadProperties props = ReadProperties.getInstance();
		email = props.readProperty("email");
		password = props.readProperty("passwd");
		host = props.readProperty("host");
		port = props.readProperty("port");
	}

	public void send() throws MessagingException, NamingException {
		Properties props = new Properties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");

		session = Session.getInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(email, password);
			}
		});

		Message msg = new MimeMessage(session);
		msg.setSubject(subject);
		msg.setFrom(new InternetAddress(email));
		msg.addRecipient(RecipientType.TO, new InternetAddress(recipient));
		BodyPart msgBodyPart = new MimeBodyPart();
		msgBodyPart.setText(message);
		Multipart multiPart = new MimeMultipart();
		multiPart.addBodyPart(msgBodyPart);
		msg.setContent(multiPart, "text/plain");
		Transport.send(msg);
	}
}
