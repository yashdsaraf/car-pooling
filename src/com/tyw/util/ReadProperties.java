package com.tyw.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class ReadProperties {

	private static final ReadProperties object = new ReadProperties();
	private Properties props;

	private ReadProperties() {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream input = loader.getResourceAsStream("env.properties");
		props = new Properties();
		try {
			props.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static ReadProperties getInstance() {
		return object;
	}

	public String readProperty(String key) {
		return props.getProperty(key);
	}

}
