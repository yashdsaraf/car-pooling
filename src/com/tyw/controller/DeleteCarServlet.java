package com.tyw.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.tyw.dao.CarDAO;
import com.tyw.daoimpl.CarDAOImpl;
import com.tyw.enums.Error;

@WebServlet("/DeleteCarServlet")
public class DeleteCarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public DeleteCarServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		String[] toReturn = new String[2];
		toReturn[0] = "ERROR";
		String carNumber = request.getParameter("carnum");
		CarDAO cars = new CarDAOImpl();
		Error result = cars.deleteCar(carNumber);
		if (result != Error.SUCCESS) {
			toReturn[1] = result.getDescription();
		} else {
			toReturn[0] = "SUCCESS";
		}
		response.getWriter().write(new Gson().toJson(toReturn));
	}

}
