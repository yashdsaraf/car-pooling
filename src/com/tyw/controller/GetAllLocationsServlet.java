package com.tyw.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonArray;
import com.tyw.dao.LocationDAO;
import com.tyw.daoimpl.LocationDAOImpl;

@WebServlet("/GetAllLocationsServlet")
public class GetAllLocationsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public GetAllLocationsServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		LocationDAO dao = new LocationDAOImpl();
		JsonArray jArray = dao.getAllLocations();
		jArray = jArray == null ? new JsonArray() : jArray;
		response.getWriter().write(jArray.toString());
	}

}
