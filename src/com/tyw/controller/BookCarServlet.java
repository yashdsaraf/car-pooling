package com.tyw.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.tyw.bean.UserBean;
import com.tyw.dao.CarPoolingDAO;
import com.tyw.dao.CarRentalDAO;
import com.tyw.dao.CustomerDAO;
import com.tyw.daoimpl.CarPoolingDAOImpl;
import com.tyw.daoimpl.CarRentalDAOImpl;
import com.tyw.daoimpl.CustomerDAOImpl;
import com.tyw.enums.Error;

@WebServlet("/BookCarServlet")
public class BookCarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		UserBean currentUser = (UserBean) request.getSession().getAttribute("currentUser");
		String[] toReturn = new String[2];
		toReturn[0] = "ERROR";
		String order_type = request.getParameter("order_type"), id = request.getParameter("id");
		CustomerDAO customerDAO = new CustomerDAOImpl();
		JsonArray jArray = null;
		if (order_type.equals("rental")) {
			CarRentalDAO dao = new CarRentalDAOImpl();
			jArray = dao.getOrderDetails(id, currentUser.getUserId());
		} else {
			CarPoolingDAO dao = new CarPoolingDAOImpl();
			jArray = dao.getOrderDetails(id, currentUser.getUserId());
		}
		if (jArray != null) {
			Error result = customerDAO.placeOrder(id, jArray);
			if (result == Error.SUCCESS) {
				toReturn[0] = "SUCCESS";
			} else {
				toReturn[1] = result.getDescription();
			}
		}
		response.getWriter().print(new Gson().toJson(toReturn));
	}

}
