package com.tyw.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.tyw.dao.CarDAO;
import com.tyw.daoimpl.CarDAOImpl;
import com.tyw.enums.Error;

@WebServlet("/ModifyPassNumServlet")
public class ModifyPassNumServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ModifyPassNumServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		String[] toReturn = new String[2];
		String carNumber = request.getParameter("carnum");
		toReturn[0] = "ERROR";
		int passnum = Integer.parseInt(request.getParameter("num"));
		CarDAO dao = new CarDAOImpl();
		Error result = dao.modifyCar(carNumber, passnum);
		if (result != Error.SUCCESS) {
			toReturn[1] = result.getDescription();
		} else {
			toReturn[0] = "SUCCESS";
		}
		response.getWriter().write(new Gson().toJson(toReturn));
	}

}
