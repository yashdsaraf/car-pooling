package com.tyw.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.tyw.bean.UserBean;

@WebServlet("/CheckSessionServlet")
public class CheckSessionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CheckSessionServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String toReturn[] = new String[2];
		String uid = request.getParameter("uid");
		toReturn[0] = "ERROR";
		HttpSession session = request.getSession();
		UserBean bean = (UserBean) session.getAttribute("currentUser");
		if (bean != null) {
			MessageDigest md = null;
			try {
				md = MessageDigest.getInstance("SHA-256");
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			byte[] hash = md.digest(bean.getUserId().getBytes(StandardCharsets.UTF_8));
			if (hash.toString().equals(uid)) {
				toReturn[0] = "SUCCESS";
				toReturn[1] = (String) session.getAttribute("role");
			} else {
				session.removeAttribute("currentUser");
				session.removeAttribute("role");
			}
		}

		if (toReturn[0] == "ERROR") {
			for (Cookie cookie : request.getCookies()) {
				cookie.setMaxAge(0);
				response.addCookie(cookie);
			}
		}

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(toReturn));
	}

}
