package com.tyw.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.tyw.dao.CarPoolingDAO;
import com.tyw.dao.CarRentalDAO;
import com.tyw.daoimpl.CarPoolingDAOImpl;
import com.tyw.daoimpl.CarRentalDAOImpl;
import com.tyw.enums.Error;

@WebServlet("/DeleteServiceServlet")
public class DeleteServiceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DeleteServiceServlet() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		String id;
		String [] toReturn = new String[2];
		toReturn[0] = "ERROR";
		Error result = null;
		if ((id = request.getParameter("rental_id")) != null) {
			CarRentalDAO dao = new CarRentalDAOImpl();
			result = dao.deleteCarRental(id);
		} else if ((id = request.getParameter("pooling_id")) != null) {
			CarPoolingDAO dao = new CarPoolingDAOImpl();
			result = dao.deleteCarPooling(id);
		}
		if (result != Error.SUCCESS) {
			toReturn[1] = result.getDescription();
		} else {
			toReturn[0] = "SUCCESS";
		}
		response.getWriter().print(new Gson().toJson(toReturn));
	}

}
