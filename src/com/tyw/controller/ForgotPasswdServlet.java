package com.tyw.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Instant;
import java.util.concurrent.ThreadLocalRandom;

import javax.mail.MessagingException;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.tyw.dao.UserDAO;
import com.tyw.daoimpl.CustomerDAOImpl;
import com.tyw.daoimpl.EmployeeDAOImpl;
import com.tyw.daoimpl.UserDAOImpl;
import com.tyw.enums.Error;
import com.tyw.util.SendEmail;

@WebServlet("/ForgotPasswdServlet")
public class ForgotPasswdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ForgotPasswdServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		String email, passwd, otp;
		String[] toReturn = new String[2];
		HttpSession session = request.getSession(true);
		passwd = request.getParameter("passwd");
		email = request.getParameter("email");
		otp = request.getParameter("otp");
		toReturn[0] = "ERROR";
		if (passwd != null) {
			UserDAO userDAO = new UserDAOImpl();
			long epoch = Instant.now().toEpochMilli();
			long otpDate = Long.parseLong(session.getAttribute("otpDate").toString());
			if (otp.equals(session.getAttribute("otp").toString())) {
				if (epoch - otpDate <= 600000) {
					Error error = userDAO.updatePasswd(email, passwd);
					if (error != Error.SUCCESS) {
						toReturn[1] = error.getDescription();
					} else {
						toReturn[0] = "SUCCESS";
					}
				} else {
					toReturn[1] = "OTP expired";
				}
			} else {
				toReturn[1] = "OTP mismatch";
			}
			session.removeAttribute("otp");
			session.removeAttribute("otpDate");
		} else {
			if (new CustomerDAOImpl().checkIfExists("email", email)
					|| new EmployeeDAOImpl().checkIfExists("email", email)) {
				int random = ThreadLocalRandom.current().nextInt(10000000, 99999999);
				String subject = "Password reset OTP";
				String body = "OTP for your TransportYourWorld account password reset request is " + "\n" + random
						+ "\n" + "Please note that the above OTP is only valid for 10 minutes.";
				try {
					new SendEmail(email, subject, body).send();
				} catch (MessagingException | NamingException e) {
					StringWriter sw = new StringWriter();
					e.printStackTrace(new PrintWriter(sw));
					toReturn[1] = sw.toString();
					response.getWriter().write(new Gson().toJson(toReturn));
				}
				session.setAttribute("otp", random);
				session.setAttribute("otpDate", Instant.now().toEpochMilli());
				toReturn[0] = "SUCCESS";
			} else {
				toReturn[1] = "The given email address does not exist";
			}
		}
		response.getWriter().write(new Gson().toJson(toReturn));
	}

}
