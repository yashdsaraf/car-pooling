package com.tyw.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.tyw.bean.UserBean;
import com.tyw.dao.CarPoolingDAO;
import com.tyw.dao.CarRentalDAO;
import com.tyw.daoimpl.CarPoolingDAOImpl;
import com.tyw.daoimpl.CarRentalDAOImpl;

@WebServlet("/GetBookingDetailsServlet")
public class GetBookingDetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public GetBookingDetailsServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		String type = request.getParameter("order_type");
		String id = request.getParameter("id");
		JsonArray jArray = new JsonArray();
		jArray.add("ERROR");
		UserBean currentUser = (UserBean) request.getSession().getAttribute("currentUser");
		if (type.equals("rental")) {
			CarRentalDAO dao = new CarRentalDAOImpl();
			jArray = dao.getOrderDetails(id, currentUser.getUserId());
			if (jArray == null) {
				return;
			}
			jArray.add(jArray.get(0));
			jArray.set(0, new JsonParser().parse("SUCCESS"));
			jArray.get(1).getAsJsonObject().addProperty("name", currentUser.getFname() + " " + currentUser.getLname());
		} else {
			CarPoolingDAO dao = new CarPoolingDAOImpl();
			jArray = dao.getOrderDetails(id, currentUser.getUserId());
			if (jArray == null) {
				return;
			}
			jArray.add(jArray.get(0));
			jArray.set(0, new JsonParser().parse("SUCCESS"));
			jArray.get(1).getAsJsonObject().addProperty("name", currentUser.getFname() + " " + currentUser.getLname());
		}
		response.getWriter().print(jArray.toString());
	}

}
