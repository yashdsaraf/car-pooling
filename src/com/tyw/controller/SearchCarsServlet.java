package com.tyw.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonArray;
import com.tyw.dao.CarPoolingDAO;
import com.tyw.dao.CarRentalDAO;
import com.tyw.daoimpl.CarPoolingDAOImpl;
import com.tyw.daoimpl.CarRentalDAOImpl;

@WebServlet("/SearchCarsServlet")
public class SearchCarsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public SearchCarsServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		String type = request.getParameter("order_type");
		JsonArray jArray = new JsonArray();
		if (type.equals("rental")) {
			CarRentalDAO dao = new CarRentalDAOImpl();
			Map<String, String> params = new HashMap<>();
			ArrayList<String> requestParams = Collections.list(request.getParameterNames());
			requestParams.remove("order_type");
			for (String param : requestParams) {
				params.put(param, request.getParameter(param));
			}
			jArray = dao.searchCars(params);
			if (jArray == null) {
				return;
			}
		} else {
			CarPoolingDAO dao = new CarPoolingDAOImpl();
			Map<String, String> params = new HashMap<>();
			ArrayList<String> requestParams = Collections.list(request.getParameterNames());
			requestParams.remove("order_type");
			for (String param : requestParams) {
				params.put(param, request.getParameter(param));
			}
			jArray = dao.searchCars(params);
			if (jArray == null) {
				return;
			}
		}
		response.getWriter().write(jArray.toString());
	}

}
