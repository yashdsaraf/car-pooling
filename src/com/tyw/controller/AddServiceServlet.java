package com.tyw.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.tyw.bean.CarPoolingBean;
import com.tyw.bean.CarRentalBean;
import com.tyw.dao.CarPoolingDAO;
import com.tyw.dao.CarRentalDAO;
import com.tyw.daoimpl.CarPoolingDAOImpl;
import com.tyw.daoimpl.CarRentalDAOImpl;
import com.tyw.enums.Error;

@WebServlet("/AddServiceServlet")
public class AddServiceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AddServiceServlet() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		String[] toReturn = new String[2];
		toReturn[0] = "ERROR";
		String type = request.getParameter("type");
		Error result = null;
		if (type.equals("rental")) {
			String location = request.getParameter("location"),
					car = request.getParameter("car"),
					fare = request.getParameter("fare");
			CarRentalDAO dao = new CarRentalDAOImpl();
			CarRentalBean bean = new CarRentalBean();
			bean.setCarNumber(car);
			bean.setFare(Double.parseDouble(fare));
			bean.setLocationId(location);
			result = dao.addService(bean);
		} else {
			String origin = request.getParameter("origin"),
					destination = request.getParameter("destination"),
					car = request.getParameter("car"),
					fare = request.getParameter("fare"),
					depdate = request.getParameter("depdate").trim(),
					deptime = request.getParameter("deptime").trim();
			CarPoolingDAO dao = new CarPoolingDAOImpl();
			CarPoolingBean bean = new CarPoolingBean();
			bean.setCarNumber(car);
			bean.setFare(Double.parseDouble(fare));
			bean.setOriginId(origin);
			bean.setDestinationId(destination);
			bean.setDeparture(depdate + " " + deptime);
			result = dao.addService(bean);
		}
		if (result != Error.SUCCESS) {
			toReturn[1] = result.getDescription();
		} else {
			toReturn[0] = "SUCCESS";
		}
		response.getWriter().write(new Gson().toJson(toReturn));
	}

}
