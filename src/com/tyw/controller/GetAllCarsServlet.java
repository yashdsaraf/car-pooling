package com.tyw.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonArray;
import com.tyw.dao.CarDAO;
import com.tyw.daoimpl.CarDAOImpl;

@WebServlet("/GetAllCarsServlet")
public class GetAllCarsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public GetAllCarsServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		CarDAO cars = new CarDAOImpl();
		JsonArray jArray = cars.getAllCars();
		jArray = jArray == null ? new JsonArray() : jArray;
		response.getWriter().write(jArray.toString());
	}

}
