package com.tyw.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.tyw.bean.LocationBean;
import com.tyw.dao.LocationDAO;
import com.tyw.daoimpl.LocationDAOImpl;
import com.tyw.enums.Error;

@WebServlet("/AddLocationServlet")
public class AddLocationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AddLocationServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		String[] toReturn = new String[2];
		toReturn[0] = "ERROR";
		String state = request.getParameter("state"), city = request.getParameter("city"),
				town = request.getParameter("town"), district = request.getParameter("district"),
				locality = request.getParameter("locality");
		LocationDAO dao = new LocationDAOImpl();
		LocationBean location = new LocationBean();
		location.setState(state);
		location.setCity(city);
		location.setDistrict(district);
		location.setTown(town);
		location.setLocality(locality);
		Error result = dao.addLocation(location);
		if (result != Error.SUCCESS) {
			toReturn[1] = result.getDescription();
		} else {
			toReturn[0] = "SUCCESS";
		}
		response.getWriter().write(new Gson().toJson(toReturn));
	}
}