package com.tyw.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonArray;
import com.tyw.dao.CarPoolingDAO;
import com.tyw.dao.CarRentalDAO;
import com.tyw.daoimpl.CarPoolingDAOImpl;
import com.tyw.daoimpl.CarRentalDAOImpl;

@WebServlet("/GetFilterValsServlet")
public class GetFilterValsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public GetFilterValsServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		String type = request.getParameter("type");
		JsonArray toReturn = null;
		if (type.equals("rental")) {
			CarRentalDAO carrental = new CarRentalDAOImpl();
			toReturn = carrental.getFilterVals();
		} else {
			CarPoolingDAO carpooling = new CarPoolingDAOImpl();
			toReturn = carpooling.getFilterVals();
		}
		if (toReturn != null) {
			response.getWriter().write(toReturn.toString());
		} else {
			response.getWriter().write("ERROR");
		}
	}

}
