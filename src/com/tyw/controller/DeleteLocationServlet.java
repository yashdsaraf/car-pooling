package com.tyw.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.tyw.dao.LocationDAO;
import com.tyw.daoimpl.LocationDAOImpl;
import com.tyw.enums.Error;

@WebServlet("/DeleteLocationServlet")
public class DeleteLocationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public DeleteLocationServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		String[] toReturn = new String[2];
		toReturn[0] = "ERROR";
		String locationId = request.getParameter("locationId");
		LocationDAO dao = new LocationDAOImpl();
		Error result = dao.deleteLocation(locationId);
		if (result != Error.SUCCESS) {
			toReturn[1] = result.getDescription();
		} else {
			toReturn[0] = "SUCCESS";
		}
		response.getWriter().write(new Gson().toJson(toReturn));
	}
}