package com.tyw.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.google.gson.Gson;
import com.tyw.bean.CarBean;
import com.tyw.dao.CarDAO;
import com.tyw.daoimpl.CarDAOImpl;
import com.tyw.enums.Error;

@WebServlet("/AddCarServlet")
@MultipartConfig
public class AddCarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String IMAGES = "assets" + File.separator + "images";

	public AddCarServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		String[] toReturn = new String[2];
		toReturn[0] = "ERROR";
		String carNum = request.getParameter("carnum"), carName = request.getParameter("name"),
				carType = request.getParameter("type"), orderType = request.getParameter("order");
		int passnum = Integer.parseInt(request.getParameter("passnum"));
		String imagePath = request.getServletContext().getRealPath(IMAGES);
		CarDAO dao = new CarDAOImpl();
		CarBean car = new CarBean();
		Part imagePart = request.getPart("image");
		try (InputStream image = imagePart.getInputStream();
				FileOutputStream imageFile = new FileOutputStream(new File(imagePath, carNum + ".jpg"))) {
			int n;
			while ((n = image.read()) != -1) {
				imageFile.write(n);
			}
		}
		car.setCarNumber(carNum);
		car.setCarImage(IMAGES + File.separator + carNum + ".jpg");
		car.setCarName(carName);
		car.setCarType(carType.trim());
		car.setOrderType(orderType.trim());
		car.setNoOfPassengers(passnum);
		Error result = dao.addCar(car);
		if (result != Error.SUCCESS) {
			toReturn[1] = result.getDescription();
		} else {
			toReturn[0] = "SUCCESS";
		}
		response.getWriter().write(new Gson().toJson(toReturn));
	}

}
