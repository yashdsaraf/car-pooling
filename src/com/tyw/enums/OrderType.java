package com.tyw.enums;

public enum OrderType {
	POOL, RENT;

	public static OrderType getOrderTypeFromString(String orderType) {
		switch (orderType.toLowerCase()) {
		case "pool":
			return OrderType.POOL;
		case "rent":
			return OrderType.RENT;
		default:
			return null;
		}
	}
}