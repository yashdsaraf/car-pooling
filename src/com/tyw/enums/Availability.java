package com.tyw.enums;

public enum Availability {
	AVAILABLE, BOOKED, EXPIRED;

	public static Availability getAvailabilityFromString(String availability) {
		switch (availability.toLowerCase()) {
		case "available":
			return Availability.AVAILABLE;
		case "booked":
			return Availability.BOOKED;
		case "expired":
			return Availability.EXPIRED;
		default:
			return null;
		}
	}
}