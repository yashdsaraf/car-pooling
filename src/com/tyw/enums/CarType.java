package com.tyw.enums;

public enum CarType {
	HATCHBACK, SEDAN, WAGON, SUV;

	public static CarType getCarTypeFromString(String type) {
		switch (type.toLowerCase()) {
		case "hatchback":
			return CarType.HATCHBACK;
		case "sedan":
			return CarType.SEDAN;
		case "wagon":
			return CarType.WAGON;
		case "suv":
			return CarType.SUV;
		default:
			return null;
		}
	}
}
