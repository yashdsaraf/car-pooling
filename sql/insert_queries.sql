-- TODO: Enter dummy data for booking, car, carrental, carpooling
insert all
into location values ('ABCDEF1243','Maharashtra','Mumbai','Thane','Borivali','Bhandarkar',NULL,NULL)
into location values ('ABCDEF1244','Maharashtra','Mumbai','South Mumbai','Churchgate','Fort',NULL,NULL)
into location values ('ABCDEF1245','Delhi','Delhi','Delhi NCR','Preet Vihar','Swasth Vihar',NULL,NULL)
into location values ('ABCDEF1246','Delhi','Delhi','Delhi NCR','New Delhi','Paschim Vihar',NULL,NULL)
into location values ('ABCDEF1247','Delhi','Delhi','Delhi NCR','New Delhi','Marhura Road',NULL,NULL)
select 1 from dual;

insert all
	INTO user_details values ('ABC123','admin1234','admin','admin','admin@123')
	INTO user_details values ('DEF123','admin3456','admin','admin','admin@123')
	INTO user_details values ('GHI123','admin5678','admin','admin','admin@123')
	INTO user_details values ('JKL123','admin7890','admin','admin','admin@123')
	INTO user_details values ('MNO123','admin9012','admin','admin','admin@123')
	INTO user_details values ('PQR123','admin0123','admin','admin','admin@123')
	INTO customer values (getUniqueVal(),'ABC123',1234567890,'admin@mail.com',0,getUniqueVal(),NULL,NULL)
	INTO customer values (getUniqueVal(),'DEF123',2234567890,'admin1@mail.com',0,getUniqueVal(),NULL,NULL)
	INTO customer values (getUniqueVal(),'GHI123',3234567890,'admin2@mail.com',0,getUniqueVal(),NULL,NULL)
	INTO customer values (getUniqueVal(),'JKL123',4234567890,'admin3@mail.com',0,getUniqueVal(),NULL,NULL)
	INTO employee values (getUniqueVal(),'MNO123','admin4@mail.com','BM','ABCDEF1243')
	INTO employee values (getUniqueVal(),'PQR123','admin5@mail.com','SM','ABCDEF1247')
 select 1 from dual;

insert all
	into car values ('CAR1111111','Maruti Suzuki Swift','HATCHBACK','RENT','AVAILABLE',4,'assets/images/1.jpg')
	into car values ('CAR2222222','Volkswagen Vento','SEDAN','RENT','AVAILABLE',4,'assets/images/2.jpg')
	into car values ('CAR3333333','Datsun Redygo','HATCHBACK','RENT','AVAILABLE',4,'assets/images/3.jpg')
	into car values ('CAR4444444','Chrevolet Beat','HATCHBACK','POOL','AVAILABLE',4,'assets/images/4.jpg')
	into car values ('CAR5555555','Maruti Suzuki Ciaz','SEDAN','POOL','AVAILABLE',4,'assets/images/5.jpg')
	into car values ('CAR6666666','Maruti Suzuki Celerio','HATCHBACK','POOL','AVAILABLE',4,'assets/images/6.jpg')
	into carrental values ('RENT111111','CAR1111111',100,'ABCDEF1243')
	into carrental values ('RENT222222','CAR2222222',200,'ABCDEF1244')
	into carrental values ('RENT333333','CAR3333333',300,'ABCDEF1245')
	into carpooling values ('POOL111111','CAR4444444',100,'ABCDEF1243','ABCDEF1244',TO_DATE('20-07-2017 03:05 PM','DD-MM-YYYY HH:MI AM'))
	into carpooling values ('POOL222222','CAR5555555',200,'ABCDEF1244','ABCDEF1243',TO_DATE('17-07-2017 03:05 PM','DD-MM-YYYY HH:MI AM'))
	into carpooling values ('POOL333333','CAR6666666',300,'ABCDEF1245','ABCDEF1246',TO_DATE('25-07-2017 03:05 PM','DD-MM-YYYY HH:MI AM'))
select 1 from dual;

commit;
