-- Delete all objects in current schema

BEGIN
   FOR cur_rec IN (SELECT object_name, object_type
                     FROM user_objects
                    WHERE object_type IN
                             ('TABLE',
                              'VIEW',
                              'PACKAGE',
                              'PROCEDURE',
                              'FUNCTION',
                              'SEQUENCE'
                             ))
   LOOP
      BEGIN
         IF cur_rec.object_type = 'TABLE'
         THEN
            EXECUTE IMMEDIATE    'DROP '
                              || cur_rec.object_type
                              || ' "'
                              || cur_rec.object_name
                              || '" CASCADE CONSTRAINTS';
         ELSE
            EXECUTE IMMEDIATE    'DROP '
                              || cur_rec.object_type
                              || ' "'
                              || cur_rec.object_name
                              || '"';
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line (   'FAILED: DROP '
                                  || cur_rec.object_type
                                  || ' "'
                                  || cur_rec.object_name
                                  || '"'
                                 );
      END;
   END LOOP;
END;
/
commit;

create table location (
	location_id VARCHAR2(10) PRIMARY KEY,
	state VARCHAR2(50),
	city VARCHAR2(50),
	district VARCHAR2(50),
	town VARCHAR2(50),
	locality VARCHAR2(50),
	latitude VARCHAR2(50),
	longitude VARCHAR2(50)
);

create table user_details (
	user_id VARCHAR2(10) PRIMARY KEY,
	login_id VARCHAR2(50) NOT NULL UNIQUE CHECK (LENGTH(login_id) >= 8),
	fname VARCHAR2(50) NOT NULL,
	lname VARCHAR2(50) NOT NULL,
	passwd VARCHAR2(50) NOT NULL CHECK (LENGTH(passwd) >= 8)
);

create table customer (
	customer_id VARCHAR2(10) PRIMARY KEY,
	user_id VARCHAR2(10),
	phone NUMBER(10) NOT NULL UNIQUE,
	email VARCHAR2(255) NOT NULL UNIQUE,
	balance NUMBER(12,4) DEFAULT 0,
	license CHAR(10) UNIQUE,
	address VARCHAR2(255),
	location_id VARCHAR2(10),
	CONSTRAINT fk_uid_customer FOREIGN KEY (user_id) REFERENCES user_details(user_id),
	CONSTRAINT fk_locid_customer FOREIGN KEY (location_id) REFERENCES location(location_id)  on delete cascade
);

create table employee (
	employee_id VARCHAR2(10) PRIMARY KEY,
	user_id VARCHAR2(10),
	email VARCHAR2(255) NOT NULL UNIQUE,
	qualification CHAR(3) NOT NULL CHECK (qualification in ('BM','TSE','SM')),
	location_id VARCHAR2(255) NOT NULL,
	CONSTRAINT fk_uid_employee FOREIGN KEY (user_id) REFERENCES user_details(user_id),
	CONSTRAINT fk_locid_employee FOREIGN KEY (location_id) REFERENCES location(location_id) on delete cascade
);

create table car (
	car_number VARCHAR2(10) PRIMARY KEY,
	car_name VARCHAR2(255) NOT NULL,
	car_type VARCHAR2(10) NOT NULL CHECK (car_type in ('HATCHBACK', 'SEDAN', 'WAGON', 'SUV')),
	order_type CHAR(4) NOT NULL CHECK (order_type in ('POOL', 'RENT')),
	availability VARCHAR2(10) DEFAULT 'AVAILABLE' NOT NULL CHECK (availability in ('AVAILABLE','BOOKED','EXPIRED')),
	no_of_passengers NUMBER(1) NOT NULL,
	car_image VARCHAR(255)
);

create table carrental (
	rental_id VARCHAR2(10) PRIMARY KEY,
	car_number VARCHAR2(10),
	fare NUMBER(8,2) DEFAULT 0 NOT NULL,
	location_id VARCHAR2(10),
	CONSTRAINT fk_carnum_carrental FOREIGN KEY (car_number) REFERENCES car(car_number) on delete cascade,
	CONSTRAINT fk_locid_carrental FOREIGN KEY (location_id) REFERENCES location(location_id) on delete cascade
);

create table carpooling (
	pooling_id VARCHAR2(10) PRIMARY KEY,
	car_number VARCHAR2(10),
	fare NUMBER(8,2) DEFAULT 0 NOT NULL,
	origin VARCHAR2(10) NOT NULL,
	destination VARCHAR2(10) NOT NULL,
	departure DATE NOT NULL,
	CONSTRAINT fk_carnum_carpool FOREIGN KEY (car_number) REFERENCES car(car_number) on delete cascade,
	CONSTRAINT fk_origin_carpool FOREIGN KEY (origin) REFERENCES location(location_id) on delete cascade,
	CONSTRAINT fk_dest_carpool FOREIGN KEY (destination) REFERENCES location(location_id) on delete cascade
);

create table booking (
	order_no VARCHAR2(10) PRIMARY KEY,
	customer_id VARCHAR2(10),
	car_number VARCHAR2(10),
	service_id VARCHAR2(10),
	order_type CHAR(4) NOT NULL CHECK (order_type in ('POOL', 'RENT')),
	booking_date DATE NOT NULL,
	end_date DATE,
	fare NUMBER(8,2) NOT NULL,
	tax NUMBER(7,2),
	tax_rate NUMBER(5,2),
	no_of_passengers NUMBER(1),
	pickup_point VARCHAR2(255),
	dropoff_point VARCHAR2(255),
	status VARCHAR2(10) CHECK (status in ('CAN','CNF','PEN','PENCAN','COMP','PENCOMP')),
	cancellation_reason VARCHAR2(255),
	CONSTRAINT fk_custid_booking FOREIGN KEY (customer_id) REFERENCES customer(customer_id)
);

create table invoice (
	invoice_no NUMBER(8) PRIMARY KEY,
	order_no VARCHAR2(10),
	customer_name VARCHAR2(255) NOT NULL,
	car_name VARCHAR2(255) NOT NULL,
	car_number VARCHAR2(10) NOT NULL,
	order_type CHAR(4) NOT NULL CHECK (order_type in ('POOL', 'RENT')),
	fare NUMBER(8,2) NOT NULL,
	tax NUMBER(7,2) NOT NULL,
	tax_rate NUMBER(5,2) NOT NULL,
	booking_date DATE NOT NULL,
	CONSTRAINT fk_ordernum_invoice FOREIGN KEY (order_no) REFERENCES booking(order_no)
);

create or replace view emp_details as
select e.*, login_id, fname, lname, passwd
from employee e join user_details u on e.user_id=u.user_id;

create or replace view cus_details as
select c.*, login_id, fname, lname, passwd
from customer c join user_details u on c.user_id=u.user_id;

create or replace view rent_details as
select fare, rental_id, availability, r.car_number, r.location_id, state, city, district, town, locality, no_of_passengers, car_type, car_name from carrental r join
car c on r.car_number=c.car_number join location l on r.location_id=l.location_id;

create or replace view pool_details as
select fare, pooling_id, availability, p.car_number, origin, destination, o.state ostate, o.city ocity, o.district odistrict, o.town otown, o.locality olocality,
d.state, d.city, d.district, d.town, d.locality, no_of_passengers, TO_CHAR(departure, 'DD-MM-YYYY') depdate, TO_CHAR(departure, 'HH:MI AM') deptime, car_type, car_name from carpooling p 
join car c on p.car_number=c.car_number
join location o on p.origin=o.location_id join location d on p.destination=d.location_id;

create or replace function getUniqueVal
return VARCHAR2
as
	begin
	return dbms_random.string('X',10);
END getUniqueVal;
/

commit;
